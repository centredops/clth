$(document).ready(function() {
 // executes when HTML-Document is loaded and DOM is ready
console.log("document is ready");
  
  
// document ready  
});


window.onload = function() {
 // executes when complete page is fully loaded, including all frames, objects and images
console.log("window is loaded");
  
  
// window load  
};

// masonry 
var $grid = $('.gallery-wrapper').masonry({
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true,
    transitionDuration: 0,
  });

  $grid.imagesLoaded().progress( function() {
    $grid.masonry();
  });


  // Hide submenus
$('#body-row .collapse').collapse('hide'); 

// Collapse/Expand icon
$('#collapse-icon').addClass('fa-angle-double-left'); 

// Collapse click
$('[data-toggle=sidebar-colapse]').click(function() {
    SidebarCollapse();
});

function SidebarCollapse () {
    $('.menu-collapsed').toggleClass('d-none');
    $('.sidebar-submenu').toggleClass('d-none');
    $('.submenu-icon').toggleClass('d-none');
    $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');
    
    // Treating d-flex/d-none on separators with title
    var SeparatorTitle = $('.sidebar-separator-title');
    if ( SeparatorTitle.hasClass('d-flex') ) {
        SeparatorTitle.removeClass('d-flex');
    } else {
        SeparatorTitle.addClass('d-flex');
    }
    
    // Collapse/Expand icon
    $('#collapse-icon').toggleClass('fa-angle-double-left fa-angle-double-right');
}


let currentValue = 1;
const timeout = 0.75;
const radios = document.querySelectorAll('.swappy-radios input');
const fakeRadios = document.querySelectorAll('.swappy-radios .radio');

//This next bit kinda sucks and could be improved.
//For simplicity, I'm assuming that the distance between the first and second radios is indicative of the distance between all radios. This will fail if one of the options goes onto two lines.
//I should really move each radio independantly depending on its own distance to its neighbour. Oh well ¯\_(ツ)_/¯
//TODO ^^^
const firstRadioY = document.querySelector('.swappy-radios label:nth-of-type(1) .radio').getBoundingClientRect().y;
const secondRadioY = document.querySelector('.swappy-radios label:nth-of-type(2) .radio').getBoundingClientRect().y;
const indicitiveDistance = secondRadioY - firstRadioY;
//End suckyness :D

//Apply CSS delays in JS, so that if JS doesn't load, it doesn't delay selected radio colour change
//I'm applying background style delay here so that it doesn't appear slow if JS is disabled/broken
fakeRadios.forEach(function(radio) {
  radio.style.cssText = `transition: background 0s ${timeout}s;`;
});
//Have to do this bit the long way (i.e. with a <style> element) becuase you can't do inline pseudo element syles
const css = `.radio::after {transition: opacity 0s ${timeout}s;}`
const head = document.head;
const style = document.createElement('style');
style.type = 'text/css';
style.appendChild(document.createTextNode(css));
head.appendChild(style);
//End no-js animation fallbacks.

radios.forEach(function(radio, i) {
  //Add an attr to make finding and styling the correct element a lot easier
  radio.parentElement.setAttribute('data-index', i + 1);
  
  //The meat: set up the change listener!
  radio.addEventListener('change', function() {
    //Stop weirdness of incomplete animation occuring. disable radios until complete.
    temporarilyDisable();

    //remove old style tag
    removeStyles();
    const nextValue = this.parentElement.dataset.index;

    const oldRadio = document.querySelector(`[data-index="${currentValue}"] .radio`);
    const newRadio = this.nextElementSibling;
    const oldRect = oldRadio.getBoundingClientRect();
    const newRect = newRadio.getBoundingClientRect();

    //Pixel distance between previous and newly-selected radios
    const yDiff = Math.abs(oldRect.y - newRect.y);
    
    //Direction. Is the new option higher or lower than the old option?
    const dirDown = oldRect.y - newRect.y > 0 ? true : false;
    
    //Figure out which unselected radios actually need to move 
    //(we don't necessarily want to move them all)
    const othersToMove = [];
    const lowEnd = Math.min(currentValue, nextValue);
    const highEnd = Math.max(currentValue, nextValue);

    const inBetweenies = range(lowEnd, highEnd, dirDown);
    let othersCss = '';
    inBetweenies.map(option => {
      //If there's more than one, add a subtle stagger effect
      const staggerDelay = inBetweenies.length > 1 ? 0.1 / inBetweenies.length * option : 0;
      othersCss += `
        [data-index="${option}"] .radio {
          animation: moveOthers ${timeout - staggerDelay}s ${staggerDelay}s;
        }
      `;
    });
    
    const css = `
      ${othersCss}
      [data-index="${currentValue}"] .radio { 
        animation: moveIt ${timeout}s; 
      }
      @keyframes moveIt {
        0% { transform: translateX(0); }
        33% { transform: translateX(-3rem) translateY(0); }
        66% { transform: translateX(-3rem) translateY(${dirDown ? '-' : ''}${yDiff}px); }
        100% { transform: translateX(0) translateY(${dirDown ? '-' : ''}${yDiff}px); }
      }
      @keyframes moveOthers {
        0% { transform: translateY(0); }
        33% { transform: translateY(0); }
        66% { transform: translateY(${dirDown ? '' : '-'}${indicitiveDistance}px); }
        100% { transform: translateY(${dirDown ? '' : '-'}${indicitiveDistance}px); }
      }
  `;
    appendStyles(css);
    currentValue = nextValue;
  });
});

function appendStyles(css) {
  const head = document.head;
  const style = document.createElement('style');
  style.type = 'text/css';
  style.id = 'swappy-radio-styles'; 
  style.appendChild(document.createTextNode(css));
  head.appendChild(style);
}
function removeStyles() {
  const node = document.getElementById('swappy-radio-styles');
  if (node && node.parentNode) {
    node.parentNode.removeChild(node);
  }
}
function range(start, end, dirDown) {
  let extra = 1;
  if (dirDown) {
      extra = 0;
  }
  return [...Array(end - start).keys()].map(v => start + v + extra);
}
function temporarilyDisable() {
    radios.forEach((item) => {
      item.setAttribute('disabled', true);
      setTimeout(() => { 
        item.removeAttribute('disabled');
      }, timeout * 1000);
    });
}

$(function(){

    $("#cart-items").slideUp();
    $(".cart").on("click", function () {
    $("#cart-items").slideToggle();
    });

    $("#items-basket").text("(" + ($("#list-item").children().length) + ")");

    
    $(".item").on("click", function () {
      $("#cart-items").slideDown();
     setTimeout(function(){
        $("#cart-items").slideUp();
     }, 2000)
      //add items to basket
      $(this).each(function () {
        var name = $(this).children(".item-detail").children("h4").text();
        var remove = "<button class='remove'> X </button>";
        var cena = "<span class='eachPrice'>" + (parseFloat($(this).children(".item-detail").children(".prices").children(".price").text())) + "</span>";
        $("#list-item").append("<li>" + name + "&#09; - &#09;" + cena + "$" + remove + "</li>");

        //number of items in basket
        $("#items-basket").text("(" + ($("#list-item").children().length) + ")");
        $("#items-basket").text();
        
          //calculate total price
          var totalPrice = 0;
            $(".eachPrice").each(function (){ 
              var cenaEach = parseFloat($(this).text());
              totalPrice+=cenaEach;
            });
            $("#total-price").text(totalPrice + "$");
      });

      //remove items from basket
      $(".remove").on("click", function () {
        $(this).parent().remove();

            var totalPrice = 0;
            $(".eachPrice").each(function (){ 
              var cenaEach = parseFloat($(this).text());
              totalPrice+=cenaEach;
            });
            $("#total-price").text(totalPrice + "$");
        $("#items-basket").text("(" + ($("#list-item").children().length) + ")");
      });
    });
})

// popovers Initialization
$(function () {
$('[data-toggle="popover"]').popover()
})


// https://dribbble.com/shots/1640899-Freebie-Course-Dashboard

$('nav a, .nav2 a').on('click', function() {
  $(this).addClass('active');  $(this).parents('li').siblings().children('a').removeClass('active');
});

$(document).ready(function () {
     if ($("[rel=tooltip]").length) {
     $("[rel=tooltip]").tooltip();
     }
   });


/*global $, console*/
/*
  By Mostafa Omar
  https://www.facebook.com/MostafaOmarIbrahiem
*/
$(function () {

  'use strict';

  (function () {

    var aside = $('.side-nav'),

        showAsideBtn = $('.show-side-btn'),

        contents = $('#contents');

    showAsideBtn.on("click", function () {

      $("#" + $(this).data('show')).toggleClass('show-side-nav');

      contents.toggleClass('margin');

    });

    if ($(window).width() <= 767) {

      aside.addClass('show-side-nav');

    }
    $(window).on('resize', function () {

      if ($(window).width() > 767) {

        aside.removeClass('show-side-nav');

      }

    });

    // dropdown menu in the side nav
    var slideNavDropdown = $('.side-nav-dropdown');

    $('.side-nav .categories li').on('click', function () {

      $(this).toggleClass('opend').siblings().removeClass('opend');

      if ($(this).hasClass('opend')) {

        $(this).find('.side-nav-dropdown').slideToggle('fast');

        $(this).siblings().find('.side-nav-dropdown').slideUp('fast');

      } else {

        $(this).find('.side-nav-dropdown').slideUp('fast');

      }

    });

    $('.side-nav .close-aside').on('click', function () {

      $('#' + $(this).data('close')).addClass('show-side-nav');

      contents.removeClass('margin');

    });

  }());

  // Start chart

  var chart = document.getElementById('myChart');
  Chart.defaults.global.animation.duration = 2000; // Animation duration
  Chart.defaults.global.title.display = false; // Remove title
  Chart.defaults.global.title.text = "Chart"; // Title
  Chart.defaults.global.title.position = 'bottom'; // Title position
  Chart.defaults.global.defaultFontColor = '#999'; // Font color
  Chart.defaults.global.defaultFontSize = 10; // Font size for every label

  // Chart.defaults.global.tooltips.backgroundColor = '#FFF'; // Tooltips background color
  Chart.defaults.global.tooltips.borderColor = 'white'; // Tooltips border color
  Chart.defaults.global.legend.labels.padding = 0;
  Chart.defaults.scale.ticks.beginAtZero = true;
  Chart.defaults.scale.gridLines.zeroLineColor = 'rgba(255, 255, 255, 0.1)';
  Chart.defaults.scale.gridLines.color = 'rgba(255, 255, 255, 0.02)';

  Chart.defaults.global.legend.display = false;

  var myChart = new Chart(chart, {
    type: 'bar',
    data: {
      labels: ["January", "February", "March", "April", "May", 'Jul'],
      datasets: [{
        label: "Lost",
        fill: false,
        lineTension: 0,
        data: [45, 25, 40, 20, 45, 20],
        pointBorderColor: "#4bc0c0",
        borderColor: '#4bc0c0',
        borderWidth: 2,
        showLine: true,
      }, {
        label: "Succes",
        fill: false,
        lineTension: 0,
        startAngle: 2,
        data: [20, 40, 20, 45, 25, 60],
        // , '#ff6384', '#4bc0c0', '#ffcd56', '#457ba1'
        backgroundColor: "transparent",
        pointBorderColor: "#ff6384",
        borderColor: '#ff6384',
        borderWidth: 2,
        showLine: true,
      }]
    },
  });
  //  Chart ( 2 )


  var Chart2 = document.getElementById('myChart2').getContext('2d');
  var chart = new Chart(Chart2, {
    type: 'line',
    data: {
      labels: ["January", "February", "March", "April", 'test', 'test', 'test', 'test'],
      datasets: [{
        label: "My First dataset",
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 79, 116)',
        borderWidth: 2,
        pointBorderColor: false,
        data: [5, 10, 5, 8, 20, 30, 20, 10],
        fill: false,
        lineTension: .4,
      }, {
        label: "Month",
        fill: false,
        lineTension: .4,
        startAngle: 2,
        data: [20, 14, 20, 25, 10, 15, 25, 10],
        // , '#ff6384', '#4bc0c0', '#ffcd56', '#457ba1'
        backgroundColor: "transparent",
        pointBorderColor: "#4bc0c0",
        borderColor: '#4bc0c0',
        borderWidth: 2,
        showLine: true,
      }, {
        label: "Month",
        fill: false,
        lineTension: .4,
        startAngle: 2,
        data: [40, 20, 5, 10, 30, 15, 15, 10],
        // , '#ff6384', '#4bc0c0', '#ffcd56', '#457ba1'
        backgroundColor: "transparent",
        pointBorderColor: "#ffcd56",
        borderColor: '#ffcd56',
        borderWidth: 2,
        showLine: true,
      }]
    },

    // Configuration options
    options: {
      title: {
        display: false
      }
    }
  });


  console.log(Chart.defaults.global);

  var chart = document.getElementById('chart3');
  var myChart = new Chart(chart, {
    type: 'line',
    data: {
      labels: ["One", "Two", "Three", "Four", "Five", 'Six', "Seven", "Eight"],
      datasets: [{
        label: "Lost",
        fill: false,
        lineTension: .5,
        pointBorderColor: "transparent",
        pointColor: "white",
        borderColor: '#d9534f',
        borderWidth: 0,
        showLine: true,
        data: [0, 40, 10, 30, 10, 20, 15, 20],
        pointBackgroundColor: 'transparent',
      },{
        label: "Lost",
        fill: false,
        lineTension: .5,
        pointColor: "white",
        borderColor: '#5cb85c',
        borderWidth: 0,
        showLine: true,
        data: [40, 0, 20, 10, 25, 15, 30, 0],
        pointBackgroundColor: 'transparent',
      },
                 {
                   label: "Lost",
                   fill: false,
                   lineTension: .5,
                   pointColor: "white",
                   borderColor: '#f0ad4e',
                   borderWidth: 0,
                   showLine: true,
                   data: [10, 40, 20, 5, 35, 15, 35, 0],
                   pointBackgroundColor: 'transparent',
                 },
                 {
                   label: "Lost",
                   fill: false,
                   lineTension: .5,
                   pointColor: "white",
                   borderColor: '#337ab7',
                   borderWidth: 0,
                   showLine: true,
                   data: [0, 30, 10, 25, 10, 40, 20, 0],
                   pointBackgroundColor: 'transparent',
                 }]
    },
  });

});

$(document).ready(function() {
  let isOpen = false;
  let $windowWidth = $( window ).width();
  const $btnCollapse = $(".button-collapse");
  const $content = $('#content');

  $( window ).resize(function() {

    $windowWidth = $( window ).width();
    if($windowWidth > 1440) {
      $content.css('padding-left', '250px');
      if(isOpen) {
        $btnCollapse.css('left', '0');
        isOpen = false;
      }
    } else if($windowWidth < 530 && isOpen) {
      $btnCollapse.css('left', '0');
      $content.css('padding-left', '0');
      $('#sidenav-overlay').css('display', 'block');
      $btnCollapse.trigger('click');
    } else {
      if(!isOpen) {
        $content.css('padding-left', '0');
      }
    }
  });

  // SideNav Button Initialization
  $btnCollapse.sideNav();

  $btnCollapse.on('click', () => {
   isOpen = !isOpen;
   if($windowWidth > 530) {
    const elPadding = isOpen ? '250px' : '0';
    $btnCollapse.css('left', elPadding);
     $content.css('padding-left', elPadding);
    $('#sidenav-overlay').css('display', 'none');
   } else {
    $('#sidenav-overlay').on('click', () => {
      isOpen = !isOpen;
    });
   }
  });
  $('#sidenav-overlay').on('click', () => {
    isOpen = !isOpen;
  });
});


// This app is sandboxed within CodePen so the referrer is always CodePen. Will work as expected when used independently.

// **Caveat** browsers will not send referrer if going to an http link from https.

$.fn.backButton = function() {
  if (document.referrer !== "") {
    $(this).show();
    $(this).on('click', function(e) {
      e.preventDefault();
      window.location.href = document.referrer;
    });
  }
}

$('.float').backButton();

