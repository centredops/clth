<?php

use Illuminate\Database\Seeder;

class CapsuleBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('capsule_brands')->insert([ 'name' => 'Puma']);
        DB::table('capsule_brands')->insert([ 'name' => 'Nike']);
        DB::table('capsule_brands')->insert([ 'name' => 'Mr Price']);
        DB::table('capsule_brands')->insert([ 'name' => 'Bathu']);
        DB::table('capsule_brands')->insert([ 'name' => 'Chepa']);
    }
}
