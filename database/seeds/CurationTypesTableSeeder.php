<?php

use Illuminate\Database\Seeder;

class CurationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('curation_types')->insert([ 'name' => 'Dresses']);
        DB::table('curation_types')->insert([ 'name' => 'Jeans']);
        DB::table('curation_types')->insert([ 'name' => 'Belts']);

    }
}
