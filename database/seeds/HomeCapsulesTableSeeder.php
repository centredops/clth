<?php

use Illuminate\Database\Seeder;

class HomeCapsulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_capsules')->insert([ 'description' => 'Lots of text here...with the four tiers of grids available you are bound to run into issues where, at certain breakpoints']);

    }
}
