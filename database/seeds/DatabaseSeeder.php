<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CurationColorsTableSeeder::class);
        $this->call(CurationCategoriesTableSeeder::class);
        $this->call(CurationListsTableSeeder::class);
        $this->call(CurationTypesTableSeeder::class);
        $this->call(CapsuleBrandsTableSeeder::class);
        $this->call(CapsuleSizesTable::class);
        $this->call(CapsuleListsTableSeeder::class);
        $this->call(HomeCurationsTableSeeder::class);
        $this->call(HomeCapsulesTableSeeder::class);
        $this->call(MerchantsTableSeeder::class);
        $this->call(UploadsTableSeeder::class);
    }
}
