<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            'name' => 'Gabriel',
            'surname' => 'Rabotlhale',
            'cell' => '0725902289',
            'file' => 'gabriel.jpg',
            'email' => 'gabriel.oft@gmail.com',
            'password' => bcrypt('gab123'),
            'role_id' => 1,
            'is_active' => 1

        ]);

        DB::table('users')->insert([

            'name' => 'Buhle',
            'surname' => 'Baruch',
            'cell' => '0672002107',
            'file' => 'buhle.jpg',
            'email' => 'buhlebaruch@gmail.com',
            'password' => bcrypt('winter'),
            'role_id' => 1,
            'is_active' => 1

        ]);
    }
}
