<?php

use Illuminate\Database\Seeder;

class CapsuleSizesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('capsule_sizes')->insert([ 'name' => 'Small']);
        DB::table('capsule_sizes')->insert([ 'name' => 'Medium']);
        DB::table('capsule_sizes')->insert([ 'name' => 'Large']);
        DB::table('capsule_sizes')->insert([ 'name' => 'Etra Large']);
    }
}
