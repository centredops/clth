<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([

            'name' => 'Administrator',
            'description' => 'Administrator has all rights to the application.'
      ]);
        DB::table('roles')->insert([

        'name' => 'Merchant',
        'description' => 'Future role for merchants to maintain their products'
    ]);
        DB::table('roles')->insert([

            'name' => 'Subscriber',
            'description' => 'This role is for default reqular website users.'
        ]);
   }
}
