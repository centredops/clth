<?php

use Illuminate\Database\Seeder;

class HomeCurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home_curations')->insert([
            'description' => 'Lots of text here...with the four tiers of grids available you are bound to run into issues where, at certain breakpoints',
            'file' => '01.jpg'
        ]);
    }
}
