<?php

use Illuminate\Database\Seeder;

class CurationCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curation_categories')->insert([

            'name' => 'Women'
        ]);

        DB::table('curation_categories')->insert([

            'name' => 'Men'
        ]);
    }
}
