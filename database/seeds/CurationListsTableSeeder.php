<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CurationListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curation_lists')->insert([

            'title' => 'Card Title 1',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-01.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 1,
            'type_id' => 1,
            'color_id' => 1,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 2',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-02.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 1,
            'type_id' => 1,
            'color_id' => 1,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 3',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-01.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 1,
            'type_id' => 1,
            'color_id' => 1,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 4',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-02.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 2,
            'type_id' => 2,
            'color_id' => 2,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 5',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-01.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 2,
            'type_id' => 2,
            'color_id' => 2,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 6',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-02.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 2,
            'type_id' => 2,
            'color_id' => 2,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 7',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-02.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 3,
            'type_id' => 3,
            'color_id' => 3,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 9',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-02.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 3,
            'type_id' => 3,
            'color_id' => 3,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);

        DB::table('curation_lists')->insert([

            'title' => 'Card Title 8',
            'description' => 'This card has supporting text below as a natural lead-in to additional content.',
            'file' => '_placeholder-02.jpg',
            'links' => '<ul><li>STYLE ITEM 1 - R900 - <strong><a href=\"https://www.mrp.com\" target=\"_blank\">Mr Price</a></strong></li><li>STYLE ITEM 2 - R400 - <strong><a href=\"https://www.yoco.co.za\" target=\"_blank\">Tshepo</a></strong></li><li>STYLE ITEM 3 - R299 - <strong><a href=\"https://www.zara.com/za/\" target=\"_blank\">ZARA</a></strong></li></ul>',
            'category_id' => 3,
            'type_id' => 3,
            'color_id' => 3,
            'is_active' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()

        ]);
    }
}
