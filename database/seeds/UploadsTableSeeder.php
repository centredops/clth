<?php

use Illuminate\Database\Seeder;

class UploadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('uploads')->insert([ 'file' => '01.jpg', 'file_id' => '1', 'type' => 'home_capsule']);
        DB::table('uploads')->insert([ 'file' => '02.jpg', 'file_id' => '1', 'type' => 'home_capsule']);
        DB::table('uploads')->insert([ 'file' => '03.jpg', 'file_id' => '1', 'type' => 'home_capsule']);
        DB::table('uploads')->insert([ 'file' => '04.jpg', 'file_id' => '1', 'type' => 'home_capsule']);

        DB::table('uploads')->insert([ 'file' => '01.jpg', 'file_id' => '2', 'type' => 'featured_merchant']);
        DB::table('uploads')->insert([ 'file' => '02.jpg', 'file_id' => '2', 'type' => 'featured_merchant']);
        DB::table('uploads')->insert([ 'file' => '03.jpg', 'file_id' => '2', 'type' => 'featured_merchant']);
        DB::table('uploads')->insert([ 'file' => '04.jpg', 'file_id' => '2', 'type' => 'featured_merchant']);

    }
}
