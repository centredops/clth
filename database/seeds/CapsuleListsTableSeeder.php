<?php

use Illuminate\Database\Seeder;

class CapsuleListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('capsule_lists')->insert([

            'title' => 'Hoodie 1',
            'details' => 'Black Lorem Sweater',
            'code' => 'SKU:12345678910',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            'file' => '1.jpg',
            'price' => '400',
            'size_id' => '32,34,36',
            'brand_id' => 1,
            'is_active' => 1

        ]);

        DB::table('capsule_lists')->insert([

            'title' => 'Hoodie 2',
            'details' => 'Pink Lorem Sweater',
            'code' => 'SKU:12345678910',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            'file' => '2.jpg',
            'price' => '500',
            'size_id' => '32,34,36',
            'brand_id' => 1,
            'is_active' => 1

        ]);

        DB::table('capsule_lists')->insert([

            'title' => 'Hoodie 3',
            'details' => 'Pink Lorem Sweater',
            'code' => 'SKU:12345678910',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            'file' => '3.jpg',
            'price' => '290',
            'size_id' => '32,34,36',
            'brand_id' => 1,
            'is_active' => 1

        ]);

        DB::table('capsule_lists')->insert([

            'title' => 'Hoodie 4',
            'details' => 'Pink Lorem Sweater',
            'code' => 'SKU:12345678910',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            'file' => '4.jpg',
            'price' => '290',
            'size_id' => '32,34,36',
            'brand_id' => 1,
            'is_active' => 1

        ]);
        DB::table('capsule_lists')->insert([

            'title' => 'Hoodie 5',
            'details' => 'Pink Lorem Sweater',
            'code' => 'SKU:12345678910',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            'file' => '5.jpg',
            'price' => '800',
            'size_id' => '32,34,36',
            'brand_id' => 1,
            'is_active' => 1

        ]);
        DB::table('capsule_lists')->insert([

            'title' => 'Hoodie 6',
            'details' => 'Pink Lorem Sweater',
            'code' => 'SKU:12345678910',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.',
            'file' => '6.jpg',
            'price' => '450',
            'size_id' => '32,34,36',
            'brand_id' => 1,
            'is_active' => 1

        ]);
    }
}
