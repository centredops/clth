<?php

use Illuminate\Database\Seeder;

class CurationColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('curation_colors')->insert([

            'name' => 'Pink',
            'hex' => '#e53bb3'
        ]);

        DB::table('curation_colors')->insert([

            'name' => 'Red',
            'hex' => '#ef2020'
        ]);
        DB::table('curation_colors')->insert([

            'name' => 'Green',
            'hex' => '#1bcc0e'
        ]);
    }
}
