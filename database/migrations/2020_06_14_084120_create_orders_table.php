<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('order_number')->nullable();
            $table->string('title')->nullable();
            $table->string('file')->nullable();
            $table->string('size')->nullable();
            $table->string('details')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->integer('quantity')->nullable();
            $table->decimal('total')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('status')->default('Pending')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
