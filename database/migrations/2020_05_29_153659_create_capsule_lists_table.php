<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapsuleListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capsule_lists', function (Blueprint $table) {
            $table->bigIncrements('id', 1);
            $table->string('title');
            $table->string('details')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->string('file')->nullable();
            $table->decimal('price')->nullable();
            $table->string('size_id')->nullable();
            $table->integer('brand_id')->unsigned()->index()->nullable();
            $table->string('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capsule_lists');
    }
}
