<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurationListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curation_lists', function (Blueprint $table) {
            $table->increments('id', 2);
            $table->string('title');
            $table->string('description')->nullable();
            $table->string('file')->nullable();
            $table->longText('links')->nullable();
            $table->integer('category_id')->unsigned()->index();
            $table->integer('type_id')->unsigned()->index();
            $table->integer('color_id')->unsigned()->index()->nullable();
            $table->string('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curation_lists');
    }
}
