<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_APP_ID', '2462514317184661'),
        'client_secret' => env('FACEBOOK_APP_SECRET', '434a8b64e8a2e9e9237a550969f576a8'),
        'redirect' => env('FACEBOOK_REDIRECT','https://clth.co.za/facebook/callback'),
    ],

    'twitter' => [
        'client_id' => env('TWITTER_APP_ID', 'joPk9pQIUir9wOsSEGwuUUCqC'),
        'client_secret' => env('TWITTER_APP_SECRET', 'YdytocRPx3434JiYnhgcWfJetqnhjUWdoDcgkFWil2uyKfmyo7'),
        'redirect' => env('TWITTER_REDIRECT','https://clth.co.za/twitter/callback'),
    ],

    'google' => [
        'client_id' => env('GOOGLE_APP_ID', '709596808012-3q020igvpeb65avq8c19asgt7jtco1ag.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_APP_SECRET', 'oJPJiJIeJI1nUFLWHhuPYezQ'),
        'redirect' => env('GOOGLE_REDIRECT','https://clth.co.za/auth/google/callback'),
    ],

];
