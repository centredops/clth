@extends('layouts.dashboard')
@section('title', 'User Profile')

@section('additional-css')
    <link href="dassets/libs/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="dassets/libs/select2/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/multiselect/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />

    <link href="dassets/libs/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
@endSection()


@section('content')
    <div class="content">

    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Pages</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">Profile</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center mt-3">
                            <img src="assets/images/users/default.jpg" alt=""
                                 class="avatar-lg rounded-circle" />
                            <h5 class="mt-2 mb-0">{{ Auth::user()->name }}</h5>
                            <h6 class="text-muted font-weight-normal mt-2 mb-0">Account Type
                            </h6>
                            <h6 class="text-muted font-weight-normal mt-1 mb-4">Super Admin</h6>

                        </div>

                        <!-- profile  -->
                        <div class="mt-5 pt-2 border-top">
                            <h4 class="mb-3 font-size-15">About</h4>
                            <p class="text-muted mb-4">Hi I'm {{ Auth::user()->name }}. I am user experience and user
                                interface designer.
                                I have been working on UI & UX since last 10 years.</p>
                        </div>
                        <div class="mt-3 pt-2 border-top">
                            <h4 class="mb-3 font-size-15">Contact Information</h4>
                            <div class="table-responsive">
                                <table class="table table-borderless mb-0 text-muted">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>{{ Auth::user()->email }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Phone</th>
                                        <td>(000) 000 0000</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address</th>
                                        <td>8907 Cosmos City, Randburg Gauteng, 2188</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="mt-3 pt-2 border-top">
                            <h4 class="mb-3 font-size-15">Tags</h4>
                            <label class="badge badge-soft-primary">Some Tag</label>
                            <label class="badge badge-soft-primary">Some Tag</label>
                            <label class="badge badge-soft-primary">Some Tag</label>
                            <label class="badge badge-soft-primary">Some Tag</label>
                            <label class="badge badge-soft-primary">Some Tag</label>
                        </div>
                    </div>
                </div>
                <!-- end card -->

            </div>

            <div class="col-lg-9">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-activity-tab" data-toggle="pill"
                                   href="#pills-activity" role="tab" aria-controls="pills-activity"
                                   aria-selected="true">
                                    My Profile
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-messages-tab" data-toggle="pill"
                                   href="#pills-messages" role="tab" aria-controls="pills-messages"
                                   aria-selected="false">
                                    My Capusules
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-projects-tab" data-toggle="pill"
                                   href="#pills-projects" role="tab" aria-controls="pills-projects"
                                   aria-selected="false">
                                    My Curations
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-tasks-tab" data-toggle="pill"
                                   href="#pills-tasks" role="tab" aria-controls="pills-tasks"
                                   aria-selected="false">
                                    My Orders
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-files-tab" data-toggle="pill"
                                   href="#pills-files" role="tab" aria-controls="pills-files"
                                   aria-selected="false">
                                    Manage Users
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-activity" role="tabpanel"
                                 aria-labelledby="pills-activity-tab">
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" name="name" placeholder="First Name">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" name="surname" placeholder="Last Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" name="name" placeholder="Telephone">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" name="surname" placeholder="Cell Number">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <textarea class="form-control" rows="2" name="description" placeholder="Delivery Address"></textarea>
                                                    </div>

                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <textarea class="form-control" rows="2" name="description" placeholder="Postal Address"></textarea>
                                                    </div>

                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" name="name" placeholder="Postal Code">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" name="surname" placeholder="Physical Code">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-body">

                                                <form action="/" method="post" class="dropzone" id="myAwesomeDropzone">
                                                    <div class="fallback">
                                                        <input name="file" type="file" multiple />
                                                    </div>

                                                    <div class="dz-message needsclick">
                                                        <i class="h1 text-muted  uil-cloud-upload"></i>
                                                        <h3>Drop files here or click to upload.</h3>
                                                        <span class="text-muted font-13">(Please drop your <strong>profile</strong> image here.</span>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                    <button class="btn btn-block btn--md btn-primary" type="submit">Update Profile</button>
                                    </div>
                                </div>

                            </div>

                            <!-- messages -->
                            <div class="tab-pane" id="pills-messages" role="tabpanel"
                                 aria-labelledby="pills-messages-tab">
                                <h5 class="mt-3">Customer's Capsules</h5>
                                <h5 class="text-center">Connecting dots...</h5>
                                <p class="text-center"><img src="{{ asset('/assets/images/404/404.png') }}" class="img-fluid block m-auto" width="400"/></p>
                                <p class="text-center">We can display recent capsules that customer has recently interacted with!</p>
                            </div>

                            <div class="tab-pane fade" id="pills-projects" role="tabpanel"
                                 aria-labelledby="pills-projects-tab">

                                <h5 class="mt-3">Customer's Interactions</h5>
                                <h5 class="text-center">Connecting dots...</h5>
                                <p class="text-center"><img src="{{ asset('/assets/images/404/404.png') }}" class="img-fluid block m-auto" width="400"/></p>
                                <p class="text-center">We can display recent curations that customer has recently interacted with!</p>
                                <!-- end attachments -->
                            </div>

                            <div class="tab-pane fade" id="pills-tasks" role="tabpanel"
                                 aria-labelledby="pills-tasks-tab">
                                <h5 class="mt-3">Customer's Order</h5>
                                <h5 class="text-center">Connecting dots...</h5>
                                <p class="text-center"><img src="{{ asset('/assets/images/404/404.png') }}" class="img-fluid block m-auto" width="400"/></p>
                                <p class="text-center">We will display all customer's orders in progress, approved and cancelled here.</p>
                                <!-- end attachments -->
                            </div>

                            <div class="tab-pane fade" id="pills-files" role="tabpanel" aria-labelledby="pills-files-tab">
                                <h5 class="mt-3">Manage users</h5>
                                <h5 class="text-center">Connecting dots...</h5>
                                <p class="text-center"><img src="{{ asset('/assets/images/404/404.png') }}" class="img-fluid block m-auto" width="400"/></p>
                                <p class="text-center">This will only be displayed to super user for managing system users.</p>
                                <!-- end attachments -->
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end card -->
            </div>
        </div>
        <!-- end row -->
    </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection

@section('additional-js')

    <!-- datatable js -->
    <script src="dassets/libs/datatables/jquery.dataTables.min.js"></script>
    <script src="dassets/libs/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="dassets/libs/datatables/dataTables.responsive.min.js"></script>
    <script src="assets/libs/datatables/responsive.bootstrap4.min.js"></script>

    <script src="dassets/libs/datatables/dataTables.buttons.min.js"></script>
    <script src="dassets/libs/datatables/buttons.bootstrap4.min.js"></script>
    <script src="dassets/libs/datatables/buttons.html5.min.js"></script>
    <script src="dassets/libs/datatables/buttons.flash.min.js"></script>
    <script src="dassets/libs/datatables/buttons.print.min.js"></script>

    <script src="dassets/libs/datatables/dataTables.keyTable.min.js"></script>
    <script src="dassets/libs/datatables/dataTables.select.min.js"></script>


    <script src="dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
    <script src="dassets/libs/select2/select2.min.js"></script>
    <script src="dassets/libs/multiselect/jquery.multi-select.js"></script>
    <script src="dassets/libs/flatpickr/flatpickr.min.js"></script>
    <script src="dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <script src="dassets/libs/dropzone/dropzone.min.js"></script>

    <!-- Datatables init -->
    <script src="dassets/js/pages/datatables.init.js"></script>
    <script src="dassets/js/pages/form-advanced.init.js"></script>

@endSection



