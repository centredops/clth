@extends('layouts.dashboard')
@section('title', 'Dashboard')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endSection()


@section('content')
    <div class="container-fluid">
        <div class="row page-title align-items-center">
            <div class="col-sm-4 col-xl-6">
                <h4 class="mb-1 mt-0">Dashboard</h4>
            </div>
            <div class="col-sm-8 col-xl-6">
                {{--<form class="form-inline float-sm-right mt-3 mt-sm-0">

                    <div class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            <i class='uil uil-file-alt mr-1'></i>Download
                            <i class="icon"><span data-feather="chevron-down"></span></i></button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item notify-item">
                                <i data-feather="printer" class="icon-dual icon-xs mr-2"></i>
                                <span>Download Orders</span>
                            </a>
                            <a href="#" class="dropdown-item notify-item">
                                <i data-feather="printer" class="icon-dual icon-xs mr-2"></i>
                                <span>Download Customers</span>
                            </a>
                            <div class="dropdown-divider"></div>

                        </div>
                    </div>
                </form>--}}
            </div>
        </div>

        <!-- content -->
        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Monthly
                                                    Revenue</span>
                                <h2 class="mb-0">R{{ $total_revenue }}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-revenue-chart" class="apex-charts"></div>
                                <span class="text-success font-weight-bold font-size-13"><i
                                            class='uil uil-arrow-up'></i> 10.21%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Total Orders</span>
                                <h2 class="mb-0">{{ $total_orders }}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-product-sold-chart" class="apex-charts"></div>
                                <span class="text-danger font-weight-bold font-size-13"><i
                                            class='uil uil-arrow-down'></i> 5.05%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Approved Orders</span>
                                <h2 class="mb-0">{{ $approved_orders }}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-new-customer-chart" class="apex-charts"></div>
                                <span class="text-success font-weight-bold font-size-13"><i
                                            class='uil uil-arrow-up'></i> 25.16%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="media p-3">
                            <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">Pending Orders</span>
                                <h2 class="mb-0">{{ $pending_orders}}</h2>
                            </div>
                            <div class="align-self-center">
                                <div id="today-new-visitors-chart" class="apex-charts"></div>
                                <span class="text-danger font-weight-bold font-size-13"><i
                                            class='uil uil-arrow-down'></i> 5.05%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- stats + charts -->
        <div class="row">
            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body p-0">
                        <h5 class="card-title header-title border-bottom p-3 mb-0">Overview</h5>
                        <!-- stat 1 -->
                        <div class="media px-3 py-4 border-bottom">
                            <div class="media-body">
                                <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $curation_likes }}</h4>
                                <span class="text-muted">Curation Likes</span>
                            </div>
                            <i data-feather="users" class="align-self-center icon-dual icon-lg"></i>
                        </div>

                        <!-- stat 2 -->
                        <div class="media px-3 py-4 border-bottom">
                            <div class="media-body">
                                <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $curation_list }}</h4>
                                <span class="text-muted">Curation List</span>
                            </div>
                            <i data-feather="image" class="align-self-center icon-dual icon-lg"></i>
                        </div>

                        <!-- stat 3 -->
                        <div class="media px-3 py-4">
                            <div class="media-body">
                                <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{ $total_users }}</h4>
                                <span class="text-muted">Active Users</span>
                            </div>
                            <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6">
                <div class="card">
                    <div class="card-body pb-0">
                        <ul class="nav card-nav float-right">
                            <li class="nav-item">
                                <a class="nav-link text-muted" href="#">Today</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-muted" href="#">7d</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="#">15d</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-muted" href="#">1m</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-muted" href="#">1y</a>
                            </li>
                        </ul>
                        <h5 class="card-title mb-0 header-title">Revenue</h5>

                        <div id="revenue-chart" class="apex-charts mt-3" dir="ltr"></div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3">
                <div class="card">
                    <div class="card-body pb-0">
                        <h5 class="card-title header-title">Targets</h5>
                        <div id="targets-chart" class="apex-charts mt-3" dir="ltr"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row -->
        <div class="row">
            <div class="col-sm-4 col-xl-6">
                <h4 class="p-tb-30">Orders</h4>
                <br>
            </div>
        </div>

        @include('.orders.order-list')


    </div>
@endSection

@section('additional-js')

    <!-- datatable js -->
    <script src="{{ asset('/dassets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dassets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('/dassets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('/dassets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/dataTables.select.min.js') }}"></script>



    <!-- Datatables init -->
    <script src="{{ asset('/dassets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ asset('dassets/js/pages/form-advanced.init.js') }}"></script>

@endSection

