@extends('layouts.front')

@section('title', 'Curation Post')

@section('additional-css')
    <meta property="og:url" content="https://clth.co.za/curation-post/{{ $curation->id }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{{$curation->title }}"/>
    <meta property="og:description" content="{{$curation->description }}"/>
    <meta property="og:image" content="https://clth.co.za//assets/images/curation/{{ $curation->file}}"/>
    <meta name="_token" content="{{csrf_token()}}"/>

@endSection


@section('content')

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v7.0"
            nonce="2FpWdz3I"></script>

    <div class="container curation-post">
        <div class="row">
            <div class="col-sm-4">
                <img src="{{ asset('/assets/images/curation/' . $curation->file) }}" alt="{{$curation->title }}"
                     class="rounded img-fluid"/>
                <div class="m-b-30"></div>
            </div>
            <div class="col-sm-7">

                <h2>{{$curation->title }}</h2>
                <p>{{$curation->description }}</p>
                <div class="small clothing-info">
                    {!! $curation->links  !!}
                </div>
                <div class="m-b-30"></div>
                <div class="wrap-like">
                    @guest
                        <a href="#" data-toggle="modal" data-target="#loginModal">
                            <button class="button rounded-circle" type="submit"><i class="far fa-heart fa-2x"></i>
                            </button>
                        </a>
                    @else
                        <form action="{{ route('likes.like') }}" method="post">
                            @csrf
                            <input type="hidden" name="curation_id" id="curation_id" value="{{ $curation->id }}"/>
                            <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}"/>
                            @if($curationLiked->isEmpty())
                                <input type="hidden" name="like" id="like" value="1"/>
                                <button class="button rounded-circle like_button" type="submit"
                                        style="background-color: transparent; color:#000;"><i
                                            class="far fa-heart fa-2x"></i></button>
                            @else
                                <input type="hidden" name="like" id="like" value="0"/>
                                <button class="button rounded-circle like_button" type="submit"
                                        style="background-color: #FF0000; color:#fff;"><i
                                            class="far fa-heart fa-2x"></i></button>
                            @endif

                        </form>
                    @endif

                    <div class="wrap-share ml-4">
                        <button class="button-share rounded-circle" data-toggle="modal" data-target="#shareModal"
                                href="#"><i class="fas fa-share-alt fa-2x"></i></button>
                    </div>
                </div>
                <a href="/curation" class="float">
                    <i class="fas fa-arrow-left"></i>
                </a>


            </div>


        </div>
    </div>


    <!-- begin::Share -->
    <div class="modal animated flipInX" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-width" role="document">
            <div class="modal-content" style="background-color: transparent; border:none;">

                <div class="modal-body">
                    <div class="container">
                        <div class="row shares">
                            <div class="col-xs-4 col-md-4">
                                <a href="http://www.facebook.com/sharer.php?u=https://clth.co.za/curation-post/{{ $curation->id }}" target="_blank">
                                    <button class="button rounded-circle" type="submit"><i
                                                class="fab fa-facebook-f fa-2x"></i></button>
                                </a>
                            </div>

                            <div class="col-xs-4 col-md-4">
                                <a href="https://twitter.com/share?url=https://clth.co.za/curation-post/{{ $curation->id }}&text={{ $curation->title}}"
                                   onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                                   target="_blank" title="{{ $curation->title}}">
                                    <button class="button rounded-circle" type="submit"><i
                                                class="fab fa-twitter fa-2x"></i></button>
                                </a>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <a href="whatsapp://send?text=https://clth.co.za/curation-post/{{ $curation->id }}"
                                   data-action="https://clth.co.za//assets/images/curation/{{ $curation->file}}"
                                   onClick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
                                   target="_blank" title="{{ $curation->title}}">
                                    <button class="button rounded-circle" type="submit"><i
                                                class="fab fa-whatsapp fa-2x"></i></button>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    </div>
    <!-- end::Share -->
@endSection

@section('additional-scripts')
    <script src="http://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".btn-submit").click(function (e) {
            e.preventDefault();
            var curation_id = $("input[name=curation_id]").val();
            var user_id = $("input[name=user_id]").val();
            var like = $("input[name=like]").val();
            $.ajax({
                type: 'POST',
                url: '/likes/post/',
                data: {name: curation_id, password: user_id, email: like},
                success: function (data) {
                    alert(data.success);
                }
            });
        });

    </script>
@endSection