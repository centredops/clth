<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order</title>
    <style type="text/css">
        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 14px;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff;
        }
    </style>
</head>
<table width="700" align="center" style="border:1px solid #000;" cellpadding="10">
    <tr>
        <td align="center">
            <img src="{{ asset('assets/images/logo-mailers.png') }}" width="100">
        </td>
    </tr>
    <tr>
        <td align="center">
            <p>Capsule Order</p>
            <p>Order Status : Pending</p>
            <p>Date Received : {{ $order_date }}</p>
            <p>Order Number : {{ $order_number }}</p>
        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="600" align="center" style="border:1px solid #000;" cellpadding="10">
                <tr>
                    <td colspan="5">
                        <p>Dear <strong>{{ Auth::user()->name }} {{ Auth::user()->surname }} </strong> , we have received your Capsule Order and it is under processing, once this have been approved you will receive notification immediately.</p>
                    </td>
                </tr>
                <tr>
                    <td><p><strong>Item:</strong></p></td>
                    <td><p><strong>Desc:</strong></p></td>
                    <td><p><strong>Code:</strong></p></td>
                    <td><p><strong>Size:</strong></p></td>
                    <td><p><strong>Quantity:</strong></p></td>
                    <td><p><strong>Price:</strong></p></td>
                </tr>
                <?php $total = 0 ?>
                @foreach($bag as $item)
                    <?php $total += $item['price'] * $item['quantity'] ?>
                <tr>
                    <td><p>{{ $item['title'] }}</p></td>
                    <td><p>{{ $item['details'] }}</p></td>
                    <td><p>{{ $item['code'] }}</p></td>
                    <td><p>{{ $item['size'] }}</p></td>
                    <td><p>{{ $item['quantity'] }}</p></td>
                    <td><p>R{{ $item['price'] }}</p></td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="6">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2" align="right"><strong>Sub Total</strong></td>
                    <td colspan="2" align="right"><strong>R{{ $total }}</strong></td>
                </tr>
                @if($total <= 450)
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2" align="right"><strong>Delivery Fee</strong></td>
                    <td colspan="2" align="right"><strong>R50</strong></td>
                </tr>
                @else
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2" align="right"><strong>Delivery Fee</strong></td>
                        <td colspan="2" align="right"><strong>Free</strong></td>
                    </tr>

                @endif
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2" align="right"><strong>Total Amount</strong></td>
                    @if($total <= 450)
                    <td colspan="2" align="right"><strong>R{{ $total + 50 }}</strong></td>
                    @else
                        <td colspan="2" align="right"><strong>R{{ $total }}</strong></td>
                    @endif
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left">
            <p>For any inquiries please email <a href="mailto:orders@clth.co.za">orders@clth.co.za</a> </p>
            <p>Copyright <?php echo date('Y') ?> | All Rights Reserved | <a href="https://clth.co.za" target="_blank"> CLTH GROUP</a> </p>
        </td>
    </tr>
</table>


<body>


</body>
</html>