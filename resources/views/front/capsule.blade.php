@extends('layouts.front')
@section('title', 'Capsules')

@section('additional-css')
    <link rel="stylesheet" href="{{ asset('/assets/plugins/capsule-list/style.css') }}">
@endSection


@section('content')
    <div class="container">
        @if($merchants->toArray())
            @foreach($merchants as $merchant)
        <div class="col-4 col-md-2">
            <img src="{{ asset('assets/images/merchants/' . $merchant->file) }}" alt="" class="img-fluid"/>
            </div>
        <div class="col-lg-8 pt-4">
            <p class="small">{{ $merchant->description }}</p> </div> </div>
       @endforeach
     @else
         <div class="col-md-12 text-center">
             <div class="m-t-50">&nbsp;</div>
             <h3>Please load featured Merchant listed</h3>
             <div class="m-t-50">&nbsp;</div>
             </div>
         </div>
    @endif
    </div>
    <div class="container">
        <div class="row m-t-30">&nbsp;</div>
        <div style="overflow: hidden; max-height: 400px;">
        @include('layouts.includes.featured-list')
        </div>
    </div>

         <div class="container">
             <div class="row m-t-30">&nbsp;</div>
        @if($capsuleLists->toArray())
        <div class="row">
            @foreach($capsuleLists as $capsule)
            <div class="col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4" onclick="location.href='/capsule-details/{{ $capsule->id }}'" data-aos="fade-down">
                <div class="card h-100">
                    <a href="#"><img src="{{ asset('/assets/images/capsules/' . $capsule->file) }}" alt="{{$capsule->title }}" class="img-fluid" /> </a>
                    <div class="card-body">
                        <p class="card-title text-center item-name-menu">
                            <a href="#">{{$capsule->title }}</a>
                        </p>
                        <p class="text-center price-menu">R{{$capsule->price }}</p>
                        <p class="card-text">{{$capsule->description }}</p>
                    </div>

                </div>
            </div>
            @endforeach
         </div>
            <div class="row">
                <div class="col-12">
                    {{ $capsuleLists->links() }}
                </div>
            </div>
          @else
            <div class="row">
                <div class="col-lg-12" onclick="location.href='/capsule-details'">
                   <h3>Watch this space, there are currently no <strong>Curations</strong> listed</h3>
                </div>
            </div>

       @endif

    </div>


@endSection

@section('additional-scripts')
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-list/wowslider.js')  }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-list/script.js')  }}"></script>
@endsection