@extends('layouts.front')
@section('title', 'Order Details')


@section('content')
    <?php $total = 0 ?>
    <div class="container">
        @if(session('bag'))
        <div>

            <h4 class="text-center mt-4">ORDER CONFIRMATION</h4>

            <h6 class="text-center pt-5">Your Order Has Been Placed Successfully!</h6>

            <p class="text-center pt-5 small">Yay! Your order has been placed and is currently being proccessed. Soon it
                it wil be on its way to you!</p>

            <p class="text-center pt-4 small">This is Your Order:</p>


            <div class="table-responsive small pb-4">
                <table class="table table-borderless mx-auto w-auto">
                    <thead>
                    <tr>
                        <th><strong>Order Item</strong></th>
                        <th><strong>Quantity</strong></th>
                        <th><strong>Price</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(session('bag') as $id => $details)
                        <?php $total += $details['price'] * $details['quantity'] ?>
                    <tr>
                        <td>{{ $details['code'] }} - {{ $details['title'] }}</td>
                        <td>1</td>
                        <td>R{{ $details['price'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td class=""><strong>Subtotal:</strong></td>
                        <td>R{{ $total }}</td>
                   @if($total <= 450)
                    <tr>
                        <td></td>
                        <td><strong>Delivery</strong></td>
                        <td>R50</td>
                    </tr>
                    @else
                        <tr>
                            <td></td>
                            <td><strong>Delivery</strong></td>
                            <td>Free</td>
                        </tr>
                    @endif
                    @if($total <= 450)
                    <tr>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>R{{ $total + 50 }}</strong></td>
                    </tr>
                    @else
                        <tr>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td><strong>R{{ $total }}</strong></td>
                        </tr>
                    @endif


                    </tbody>
                </table>
                </table> </div>


            <p class="text-center pt-2 small"> Thank You For Shopping With Clth and we hope you love your garment
            </p>


            <div class="col text-center">
                <a class="btn btn-dark" href="{{ route('front.continue-shopping') }}" role="button">Continue Shoppping</a> <a class="btn btn-dark" href="{{ route('front.track-order') }}"
                                                                                         role="button">Track Order</a>
            </div>

        </div>

@endif
    </div>
@endSection

