@extends('layouts.front')
@section('title', 'Capsules Details')

@section('additional-css')
    <link rel="stylesheet" href="{{ asset('/assets/plugins/capsule-gallery/style.css') }}">
@endSection

@section('content')
    <div class="container">
        <div class="row">
           <div class="col-md-6">
              @if($capsuleGallery->isEmpty())
                    <img src="{{ asset('/assets/images/capsules/' . $capsuleDetails->file) }}" alt="{{$capsuleDetails->title }}" class="img-fluid"/>
               @else

                    <div id="wowslider-container1">
                        <div class="ws_images">
                            <ul>
                                @foreach($capsuleGallery as $galleryImage)
                                <li><img src="{{ asset('assets/images/capsule-gallery/' . $galleryImage->file) }}" alt="Capsule Gallery" id="{{ $galleryImage->id }}"/></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="ws_bullets">
                            <div>
                                @foreach($capsuleGallery as $galleryImage)
                                <a href="#" title="{{ $galleryImage->id }}<"><span>{{ $galleryImage->id }}</span></a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif

               <div class="m-t-25">&nbsp;</div>

            </div>
            <div class="col-md-6 my-md-5">
                <form action="{{ route('front.add-shopping-bag', $capsuleDetails->id ) }}">
                 @csrf


                <h2>{{$capsuleDetails->title }}</h2>
                <h6 class="small">{{ $capsuleDetails->details }}</h6>
                <p>{{ $capsuleDetails->code }}</p>
                <p class="small product-description">{{ $capsuleDetails->description }}</p>
                <p class="small product-price2"><strong> R{{ $capsuleDetails->price }}</strong></p>

                <div class="mx-auto">
                        <select name="size" class="browser-default custom-select-sm small">
                        <option value="" selected disabled hidden>Select Size</option>
                        @foreach($sizes as $size)
                            <option value="{{ $size }}">{{ $size }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="row">
                   <div class="col-md-12">
                    <input type="hidden" name="title" value="{{$capsuleDetails->title }}">
                    <input type="hidden" name="details" value="{{$capsuleDetails->details }}">
                    <input type="hidden" name="code" value="{{$capsuleDetails->code }}">
                    <input type="hidden" name="description" value="{{$capsuleDetails->description }}">
                    <input type="hidden" name="file" value="{{$capsuleDetails->file }}">
                    <input type="hidden" name="price" value="{{ $capsuleDetails->price }}">
                    @guest
                        <a href="#" data-toggle="modal" data-target="#loginModal"><button type="submit" class="btn btn-dark small">Add To Bag</button></a>
                    @else
                        <button type="submit" class="btn btn-dark small">Add To Bag</button>
                    @endif
                 </div>



                </div>
               <div class="share-social-product mx-auto"><a href=""><i class="fab fa-facebook-f"></i></a><a href=""><i class="fab fa-twitter px-3" ></i></a><a href=""><i class="fab fa-instagram"></i></a></div>

                </form>
                <a href="/capsule" class="float">
                    <i class="fas fa-arrow-left"></i>
                </a>
            </div>

        </div>
    </div>
@endSection

@section('additional-scripts')
        <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-gallery/wowslider.js')  }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-gallery/script.js')  }}"></script>
@endSection