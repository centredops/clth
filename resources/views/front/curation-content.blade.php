<div class="infinite-scroll">
<div class="masonry">
    @foreach($curations as $curation)
        <div class="item hvr-push">
            <a href="/curation-post/{{ $curation->id }}">
                <img class="card-img-top img-fluid rounded" src="{{ asset('/assets/images/curation/' . $curation->file) }}" alt="{{ $curation->title }}"></a>
            <div class="card-body">
                <br>
                <h4 class="card-title">{{ $curation->title }}</h4>
                <p class="card-text">  {{$curation->description }}</p>
                <p class="card-text"><small class="text-muted">Last updated {{$curation->updated_at->diffForHumans()}}</small></p>
                <br>
            </div>
        </div>
    @endforeach


</div>
</div>
