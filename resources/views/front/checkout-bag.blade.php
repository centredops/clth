@extends('layouts.front')
@section('title', 'Checkout Bag | Capsules')

@section('additional-css')


@endSection


@section('content')
    <div class="container">
        <form action="{{route('front.place-order',Auth::user()->id ) }}">
            @csrf

            <input type="hidden" value="{{ Auth::user()->id }}" name="user_id"/>
            <div class="row">
                <div class="wrap cf" style="width:100%; padding:0;">
                    <h6 class="projTitle">CHECK OUT</h6>

                    <div class="container">
                        <div class="m-t-50"></div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="name">Name</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}"
                                           placeholder="{{ Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="surname">Surname</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="surname"
                                           value="{{ Auth::user()->surname }}"
                                           placeholder="{{ Auth::user()->surname }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="address">Street Address</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="address" value="{{ $customer_details[0]->address }}">

                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="city">City</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="city" value="{{ $customer_details[0]->city }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="postal_code">Postal Code</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="postal_code" value="{{ $customer_details[0]->postal_code }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="province">Province</label>
                                <div class="col-lg-12">
                                    <select class="form-control" name="province">
                                        <option>Gauteng</option>
                                        <option>Western Cape</option>
                                        <option>Kwa-Zulu Natal</option>
                                        <option>North West</option>
                                        <option>Eastern Cape</option>
                                        <option>Mpumalanga</option>
                                        <option>Limpopo</option>
                                        <option>Frhjhee State</option>
                                        <option>North West</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="code">Email</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="email"
                                           value="{{ Auth::user()->email }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-lg-12" for="city">Phone Number</label>
                                <div class="col-lg-12">
                                    <input type="text" class="form-control" name="cell"
                                           value="{{ Auth::user()->cell }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-12">
                                <h6 class="text-center small pt-4">Payment Method</h6>
                                <hr class="">

                                <input type="radio" id="test1" name="payment_method" value="CASH" checked>
                                <label for="test1" class="small">Cash&nbsp&nbspOn&nbspDelivery(COD)</label>
                                <br/>
                                <br/>
                                <input type="radio" id="test2" name="payment_method" value="EFT">
                                <label for="test2" class="small">Instant&nbspEFT&nbspwith&nbspOzow</label>
                                <br/>
                                <br/>
                            </div>
                        </div>

                        <p class="small">Note: Currently, Clth only offers payment options of <a href="#">Cash On
                                Delivery(COD)</a> & <a href="#">Instant EFT</a>. We charge a handling fee of R10 for
                            Cash On Delivey while EFT has no handling fee <a href="#">Ozow</a> <br> <br> For more
                            information please see our <a href="#">Payments Information</a></p>

                    </div>


                    <h6 class="text-center small mt-5">Order Summary</h6>
                    <hr class="">
                    <div class="cart">
                        <ul class="cartWrap">

                            <?php $total = 0 ?>

                            @if(session('bag'))
                                @foreach(session('bag') as $id => $details)
                                    <?php $total += $details['price'] * $details['quantity'] ?>
                                    <li class="items">

                                        <div class="infoWrap">
                                            <div class="cartSection">
                                                <img src="{{ asset('assets/images/capsules/'. $details['file']) }}"
                                                     width="113" alt="{{ $details['title'] }}" class="itemImg"/>
                                                <p class="itemNumber">{{ $details['code'] }}</p>
                                                <h3>{{ $details['title'] }}</h3>

                                                <p><input type="text" name="quality" class="qty"
                                                          placeholder="{{ $details['quantity'] }}" disabled/> x
                                                    R{{ $details['price'] }}</p>

                                                <p class="stockStatus d-none"> In Stock</p>
                                            </div>
                                            <div class="prodTotal cartSection">
                                                <p>R{{ $details['price'] }}</p>
                                            </div>
                                            <div class="cartSection removeWrap">
                                                <a href="#" class="remove remove-shopping-bag" data-id="{{ $id }}">x</a>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>

                    <div class="promoCode d-none"><label for="promo">Have A Promo Code?</label>
                        <input type="text" name="promo" placholder="Enter Code"/>
                        <a href="#" class="btn"></a></div>

                    <div class="subtotal cf">
                        <ul>
                            <li class="totalRow "><span class="label">Subtotal</span><span
                                        class="value">R{{ $total }}</span></li>

                            @if($total <= 450)
                                <li class="totalRow "><span class="label">Delivery Fee</span><span
                                            class="value">R50</span></li>
                            @else
                                <li class="totalRow "><span class="label">Delivery Fee</span><span
                                            class="value">Free</span></li>
                            @endif

                            <li class="totalRow" style="display: none"><span class="label">Shipping</span><span
                                        class="value">R0.00</span></li>

                            <li class="totalRow" style="display: none"><span class="label">VAT</span><span
                                        class="value">R110.00</span>
                            </li>
                            @if($total <= 450)
                            <li class="totalRow final"><span class="label">Total</span><span
                                        class="value">R{{ $total + 50}}</span></li>
                            @else
                                <li class="totalRow final"><span class="label">Total</span><span
                                            class="value">R{{ $total }}</span></li>
                            @endif
                            <li class="totalRow">
                                <button type="submit" class="btn btn-secondary">Complete Order</button>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </form>
    </div>
@endSection

@section('additional-scripts')

@endSection