@extends('layouts.front')
@section('title', 'Business')


@section('content')
    <div class="container">

        <div class="col-md-12 text-center">
            <h1>CLTH Business</h1>

        </div>

        <div class="col-md-12">
            @if($content->isEmpty())
                <h4 class="text-center">There is currently no content loaded on CMS <br> <strong>Buhle Update</strong></h4>
            @else
                @foreach($content as $data)
                    {!! $data->content !!}

                @endforeach
            @endif
        </div>


    </div>

@endSection

