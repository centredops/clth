@extends('layouts.front')
@section('title', 'Shopping Closet')

@section('additional-css')


@endSection


@section('content')
    <div class="container-fluid">
        <h4 class="text-center myheader mt-10">ACCOUNT DETAILS</h4>
        <br>
        <p class="text-center small">{{ Auth::user()->name }} {{ Auth::user()->surname }}</p>
        <div class="row">
            <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2">
                <div class="nav flex-column nav-pills black" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Closet</a>
                    <a class="nav-link" id="v-pending-orders-tab" data-toggle="pill" href="#v-pending-orders" role="tab" aria-controls="v-pending-orders" aria-selected="false">Pending Orders</a>
                    <a class="nav-link" id="v-order-history-tab" data-toggle="pill" href="#v-order-history" role="tab" aria-controls="v-order-history" aria-selected="false">Order History</a>
                    <a class="nav-link" id="v-cancelled-orders-tab" data-toggle="pill" href="#v-cancelled-orders" role="tab" aria-controls="v-cancelled-orders" aria-selected="false">Cancelled Orders</a>
                    <a class="nav-link" id="v-delivery-details-tab" data-toggle="pill" href="#v-delivery-details" role="tab" aria-controls="v-delivery-details" aria-selected="false">Delivery Details</a>
                    <a class="nav-link" id="v-personal-details-tab" data-toggle="pill" href="#v-personal-details" role="tab" aria-controls="v-personal-details" aria-selected="false">Personal Details</a>
                </div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8 col-xl-8">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <h3 class="font-weight-light text-center text-lg- mt-4 mb-0">YOUR STYLE CLOSET</h3>
                        <div class="m-b-30"></div>
                        <hr class="mt-2 mb-5">
                        @if($curations->isEmpty())
                            <h4 class="text-center">Your <strong>Style Closet</strong> is empty!</h4>
                        @else
                        <div class="row">
                              @foreach($curations as $curation)

                               <div class="col-6 col-xs-6 col-md-3" onclick="location.href='/curation-post/{{ $curation->id }}'">
                                <span class="d-block mb-4 h-100">
                                    <img src="{{ asset('/assets/images/curation/' . $curation->file) }}" class="img-fluid img-thumbnail" align="{{ $curation->title }}" />
                                </span>
                            </div>

                            @endforeach
                           <div class="col-12">{{ $curations->links() }}</div>
                        </div>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="v-pending-orders" role="tabpanel" aria-labelledby="v-pending-orders-tab">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="font-weight-light text-center text-lg- mt-4 mb-0">PENDING ORDERS</h3>
                                <div class="m-b-30"></div>
                                <hr class="mt-2 mb-5">
                                @if($orders->isEmpty())
                                    <h4 class="text-center">You have no <strong>Pending Orders.</strong></h4>
                                @else

                            <table id="basic-datatable" class="table dt-responsive nowrap" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>Order Number</th>
                                    <th>Date Created</th>
                                    <th>Order Status</th>
                                    <th>Amount</th>
                                    <th>Payment Method</th>
                                </tr>
                                </thead>


                                <tbody>
                                @foreach($orders as $order)
                                    <tr>
                                        <td>{{ $order->order_number }}</td>
                                        <td>{{ $order->created_at }}</td>
                                        <td>{{ $order->status }}</td>
                                        <td>R{{ $order->total }}</td>
                                        <td>{{ $order->payment_method}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="v-order-history" role="tabpanel" aria-labelledby="v-order-history-tab">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="font-weight-light text-center text-lg- mt-4 mb-0">ORDER HISTORY</h3>
                                <div class="m-b-30"></div>
                                <hr class="mt-2 mb-5">
                                @if($order_history->isEmpty())
                                    <h4 class="text-center">You have no <strong>Order History.</strong></h4>
                                @else
                                    <table id="basic-datatable" class="table dt-responsive nowrap" style="width: 100%;">
                                        <thead>
                                        <tr>
                                            <th>Order Number</th>
                                            <th>Date Created</th>
                                            <th>Order Status</th>
                                            <th>Amount</th>
                                            <th>Payment Method</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($order_history as $order)
                                            <tr>
                                                <td>{{ $order->order_number }}</td>
                                                <td>{{ $order->created_at }}</td>
                                                <td>{{ $order->status }}</td>
                                                <td>R{{ $order->total }}</td>
                                                <td>{{ $order->payment_method}}</td>
                                            </tr>



                                        @endforeach
                                        </tbody>
                                    </table>
                                    {{ $order_history->links() }}
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="v-cancelled-orders" role="tabpanel" aria-labelledby="v-cancelled-orders">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="font-weight-light text-center text-lg- mt-4 mb-0">CANCELLED ORDERS</h3>
                                <div class="m-b-30"></div>
                                <hr class="mt-2 mb-5">
                                @if($cancelled_orders->isEmpty())
                                    <h4 class="text-center">You have no <strong>Cancelled Orders.</strong></h4>
                                @else
                                    <table id="basic-datatable" class="table dt-responsive nowrap" style="width: 100%;">
                                        <thead>
                                        <tr>
                                            <th>Order Number</th>
                                            <th>Date Created</th>
                                            <th>Order Status</th>
                                            <th>Amount</th>
                                            <th>Payment Method</th>
                                        </tr>
                                        </thead>


                                        <tbody>
                                        @foreach($cancelled_orders as $order)
                                            <tr>
                                                <td>{{ $order->order_number }}</td>
                                                <td>{{ $order->created_at }}</td>
                                                <td>{{ $order->status }}</td>
                                                <td>R{{ $order->total }}</td>
                                                <td>{{ $order->payment_method}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="v-delivery-details" role="tabpanel" aria-labelledby="v-delivery-details">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="font-weight-light text-center text-lg- mt-4 mb-0">DELIVERY DETAILS</h3>
                                <hr class="mt-2">
                                <form action="{{route('front.update-customer', Auth::user()->id ) }}" >
                                    @csrf
                                   <input type="hidden" value="{{ Auth::user()->id }}" name="user_id"/>
                                    <div class="row">
                                        <div class="wrap cf" style="width:100%; padding:0;">
                                            <div class="container">
                                                <div class="m-t-50"></div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="name">Name</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}"
                                                                   placeholder="{{ Auth::user()->name }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="surname">Surname</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="surname"
                                                                   value="{{ Auth::user()->surname }}"
                                                                   placeholder="{{ Auth::user()->surname }}" readonly>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="address">Street Address</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="address" value="{{ $customer_details[0]->address }}">

                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="city">City</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="city" value="{{ $customer_details[0]->city }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="postal_code">Postal Code</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="postal_code" value="{{ $customer_details[0]->postal_code }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="province">Province</label>
                                                        <div class="col-lg-12">
                                                            <select class="form-control" name="province">
                                                                <option>Gauteng</option>
                                                                <option>Western Cape</option>
                                                                <option>Kwa-Zulu Natal</option>
                                                                <option>North West</option>
                                                                <option>Eastern Cape</option>
                                                                <option>Mpumalanga</option>
                                                                <option>Limpopo</option>
                                                                <option>Frhjhee State</option>
                                                                <option>North West</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="code">Email</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="email"
                                                                   value="{{ Auth::user()->email }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="city">Phone Number</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="cell"
                                                                   value="{{ Auth::user()->cell }}" readonly>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <h6 class="text-center small pt-4">Payment Method</h6>
                                                        <hr class="">

                                                        <input type="radio" id="test1" name="payment_method" value="CASH" checked>
                                                        <label for="test1" class="small">Cash&nbsp&nbspOn&nbspDelivery(COD)</label>
                                                        <br/>
                                                        <br/>
                                                        <input type="radio" id="test2" name="payment_method" value="EFT">
                                                        <label for="test2" class="small">Instant&nbspEFT&nbspwith&nbspOzow</label>
                                                        <br/>
                                                        <br/>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-secondary">Update Details</button>
                                                    </div>
                                                </div>
                                           </div>




                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane fade" id="v-personal-details" role="tabpanel" aria-labelledby="v-personal-details">
                        <div class="row">
                            <div class="col-12">
                                <h3 class="font-weight-light text-center text-lg- mt-4 mb-0">PERSONAL DETAILS</h3>
                                <hr class="mt-2">
                                <form action="{{route('front.my-profile', Auth::user()->id ) }}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{ Auth::user()->id }}" name="user_id"/>
                                    <div class="row">
                                        <div class="wrap cf" style="width:100%; padding:0;">
                                            <div class="container">
                                                <div class="m-t-50"></div>
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="name">Name</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}"
                                                                   placeholder="{{ Auth::user()->name }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="surname">Surname</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="surname"
                                                                   value="{{ Auth::user()->surname }}"
                                                                   placeholder="{{ Auth::user()->surname }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="name">Email Address</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="email" value="{{ Auth::user()->email }}"
                                                                   placeholder="{{ Auth::user()->email }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label class="col-lg-12" for="surname">Cell Number</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="cell"
                                                                   value="{{ Auth::user()->cell }}"
                                                                   placeholder="{{ Auth::user()->cell }}">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <label class="col-lg-12" for="name">Confirm Password</label>
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="password"
                                                                   placeholder="Confirm Password">
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn-secondary">Update Details</button>
                                                    </div>
                                                </div>
                                            </div>




                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endSection

