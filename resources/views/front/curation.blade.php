<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Curation | CLTH </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon/favicon.ico') }}">
    <meta name="robots" content="all,follow">
    <link href="{{ asset('/assets/plugins/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('/css/defaults.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/bootstrap/413/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/masonry/css/stylesheet.css') }}">
    <style type="text/css">
        .form-check-label {
            margin-bottom: 10px;
            white-space: nowrap;
        }

        @media screen and (max-width: 320px){
            .masonry .item { width:49%;}
        }
    </style>

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script>
        $(document).ready(function(){
            $("#hide_mobile_search").click(function(){
                $(".mobile-search").hide();
            });
            $("#show_mobile_search").click(function(){
                $(".mobile-search").show();
            });
        });
    </script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script type="text/javascript">


        $(function () {
            $('.selected_category').click(function () {
                var selectedCategory = [];

                $('.selected_category').each(function () {
                    if ($(this).is(":checked")) {

                        selectedCategory.push($(this).val());
                    }
                });
                Finalcategory = selectedCategory.toString();

                $.ajax({
                    type: 'get',
                    dataType: 'html',
                    url: '/curation/content-content',
                    data: 'selectedCategory=' + Finalcategory,
                    success: function (response) {
                        $('.ajax_filter_div').html(response);
                    }
                });

            });
        });

        $(function () {
            $('.selected_type').click(function () {
                var selectedType = [];

                $('.selected_type').each(function () {
                    if ($(this).is(":checked")) {

                        selectedType.push($(this).val());
                    }
                });
                Finaltype = selectedType.toString();

                $.ajax({
                    type: 'get',
                    dataType: 'html',
                    url: '/curation/content-content',
                    data: 'selectedType=' + Finaltype,
                    success: function (response) {
                        $('.ajax_filter_div').html(response);
                    }
                });

            });
        });


        $(function () {
            $('.selected_color').click(function () {

                var selectedColor = [];

                $('.selected_color').each(function () {
                    if ($(this).is(":checked")) {

                        selectedColor.push($(this).val());
                    }
                });
                Finalcolor = selectedColor.toString();

                $.ajax({
                    type: 'get',
                    dataType: 'html',
                    url: '/curation/content-content',
                    data: 'selectedColor=' + Finalcolor,
                    success: function (response) {
                        $('.ajax_filter_div').html(response);
                    }
                });

            });
        });

    </script>
    <!-- end::Additional Stylesheet -->

    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>

<!--begin: top nav -->

<div class="m-t-10"></div>
@include('layouts/includes/top-nav')
<div class="m-t-40"></div>
<!--end: top nav -->

<!--begin:: content -->
<div class="container-fluid">
    @include('layouts.includes.success')
    @include('layouts.includes.errors')
</div>


<div class="container-fluid filters_nav">
    <div class="row">
        <div class="col-lg-2 col-md-3 col-sm-3 d-block d-sm-none remove_on_575 animated slideInRight">
            <div id="accordion">
                <div class="card">

                    <button class="btn" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                            aria-controls="collapseOne">
                        Filter According To:
                    </button>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">

                            <div class="col-12">
                                <!-- begin: Filter with category -->
                                @if($colors->toArray())
                                    <div class="ml-3 mt-4 small">

                                        @foreach($categories as $category)
                                            <input class="radio-custom selected_category"
                                                   id="categoryId-{{$category->id}}" name="radio" type="radio"
                                                   value="{{$category->id}}">
                                            <label for="categoryId-{{$category->id}}" class="radio-custom-label"
                                                   style="background-color: transparent">{{ucwords($category->name)}}
                                                &nbsp;</label>
                                        @endforeach
                                        <input class="radio-custom selected_category" id="categoryId-3" name="radio"
                                               type="radio" value="1,2">
                                        <label for="categoryId-3" class="radio-custom-label"
                                               style="background-color: transparent">Both</label>
                                        <div class="form-check list-inline checkbox "
                                             style="color:#fff; display: none;">
                                            <label class="form-check-label" for="check1" style="color:#fff;">
                                                <input type="checkbox" class="selected_category" id="default" value="0"
                                                       checked="checked" style="color:#fff;">Default
                                            </label>
                                        </div>
                                    </div>
                            @endif
                            <!-- end: Filter with category -->
                            </div>

                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-12">
                                <!-- begin: Filter with Type -->
                                @if($types->toArray())
                                    <p class="small ml-3">Curation Item</p>

                                    <div class="list-inline">
                                        <div class="form-check list-inline checkbox">
                                            <label class="form-check-label" for="check1">
                                                <input type="checkbox" class="form-check-input selected_type"
                                                       id="brandId" value="1,2,3,4,5,6,8,9">All
                                            </label>
                                        </div>

                                        @foreach($types as $type)
                                            <div class="form-check">
                                                <label class="form-check-label" for="check2">
                                                    <input type="checkbox" class="form-check-input selected_type"
                                                           id="brandId" value="{{$type->id}}">{{ucwords($type->name)}}
                                                </label>
                                            </div>
                                        @endforeach
                                        <div class="form-check list-inline checkbox "
                                             style="color:#fff; display: none;">
                                            <label class="form-check-label" for="check1" style="color:#fff;">
                                                <input type="checkbox" class="form-check-input selected_type"
                                                       id="default" value="0" checked="checked" style="color:#fff;">Default
                                            </label>
                                        </div>
                                    </div>
                            @endif
                            <!-- end: Filter with Type -->
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <!-- begin: Filter with color -->
                                    @if($colors->toArray())
                                        <div class="">
                                            <p class="small" data-toggle="collapse" data-target="#collapseExample"
                                               aria-expanded="false" aria-controls="collapseExample">
                                                Curation Colors</p>
                                            <div class="" id="collapseExample">
                                                @foreach($colors as $color)
                                                    <input id="{{$color->id}}" class="checkbox-custom selected_color"
                                                           id="colorId" name="checkbox-1" type="checkbox"
                                                           value="{{$color->id}}"
                                                           style="background-color:{{$color->hex }}">
                                                    <label for="{{$color->id}}" class="checkbox-custom-label"
                                                           style="background:{{ $color->hex }}"></label>
                                                @endforeach
                                                <div class="form-check list-inline checkbox "
                                                     style="color:#fff; display: none;">
                                                    <label class="form-check-label" for="check1" style="color:#fff;">
                                                        <input type="checkbox" class="selected_color" id="default"
                                                               value="0" checked="checked" style="color:#fff;">Default
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                @endif
                                <!-- end: Filter with color -->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-2 col-md-3 col-sm-3 col-xl-2 d-none d-sm-block animated slideInRight">
            <div id="sidebar-container" class="sidebar-expanded">
                <!-- Filter Begins -->
                <li class="text-muted d-flex align-items-center small ml-3 mt-5 menu-collapsed">
                    <small>Filter According To:</small>
                </li>

                <!-- begin: Filter with category -->
                @if($colors->toArray())
                    <div class="ml-3 mt-4 small">

                        @foreach($categories as $category)
                            <input class="radio-custom selected_category" id="categoryId-{{$category->id}}" name="radio"
                                   type="radio" value="{{$category->id}}">
                            <label for="categoryId-{{$category->id}}" class="radio-custom-label"
                                   style="background-color: transparent">{{ucwords($category->name)}}&nbsp;</label>
                        @endforeach
                        <input class="radio-custom selected_category" id="categoryId-3" name="radio" type="radio"
                               value="1,2">
                        <label for="categoryId-3" class="radio-custom-label"
                               style="background-color: transparent">Both</label>
                        <div class="form-check list-inline checkbox " style="color:#fff; display: none;">
                            <label class="form-check-label" for="check1" style="color:#fff;">
                                <input type="checkbox" class="selected_category" id="default" value="0"
                                       checked="checked" style="color:#fff;">Default
                            </label>
                        </div>
                    </div>
                @endif
            <!-- end: Filter with category -->

                <!-- begin: Filter with Type -->
                @if($types->toArray())
                    <p class="small ml-3">Curation Item</p>

                    <div class="ml-3 mt-2 list-inline">
                        <div class="form-check list-inline checkbox">
                            <label class="form-check-label" for="check1">
                                <input type="checkbox" class="form-check-input selected_type" id="brandId"
                                       value="1,2,3,4,5,6,8,9">All
                            </label>
                        </div>

                        @foreach($types as $type)
                            <div class="form-check">
                                <label class="form-check-label" for="check2">
                                    <input type="checkbox" class="form-check-input selected_type" id="brandId"
                                           value="{{$type->id}}">{{ucwords($type->name)}}
                                </label>
                            </div>
                        @endforeach
                        <div class="form-check list-inline checkbox " style="color:#fff; display: none;">
                            <label class="form-check-label" for="check1" style="color:#fff;">
                                <input type="checkbox" class="form-check-input selected_type" id="default" value="0"
                                       checked="checked" style="color:#fff;">Default
                            </label>
                        </div>
                    </div>
                @endif
            <!-- end: Filter with Type -->

                <!-- begin: Filter with color -->
                @if($colors->toArray())
                    <div class="ml-3 mt-4 small">
                        <p class="small" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                           aria-controls="collapseExample">
                            Curation Colors</p>
                        <div class="collapse" id="collapseExample">
                            @foreach($colors as $color)
                                <input id="{{$color->id}}" class="checkbox-custom selected_color" id="colorId"
                                       name="checkbox-1" type="checkbox" value="{{$color->id}}"
                                       style="background-color:{{$color->hex }}">
                                <label for="{{$color->id}}" class="checkbox-custom-label"
                                       style="background:{{ $color->hex }}"></label>
                            @endforeach
                            <div class="form-check list-inline checkbox " style="color:#fff; display: none;">
                                <label class="form-check-label" for="check1" style="color:#fff;">
                                    <input type="checkbox" class="selected_color" id="default" value="0"
                                           checked="checked" style="color:#fff;">Default
                                </label>
                            </div>
                        </div>
                    </div>
            @endif
            <!-- end: Filter with color -->

            </div>
        </div>
        @if($curations)

            <div class="col-lg-8 col-12 col-md-9 col-sm-9 ajax_filter_div">
                <div class="infinite-scroll">
                    <div class="masonry ">
                        @foreach($curations as $curation)
                            <div class="item hvr-push">
                                <a href="/curation-post/{{ $curation->id }}">
                                    <img class="card-img-top img-fluid rounded"
                                         src="{{ asset('/assets/images/curation/' . $curation->file) }}"
                                         alt="{{ $curation->title }}"></a>
                                <div class="card-body">
                                    <br>
                                    <h4 class="card-title">{{ $curation->title }}</h4>
                                    <p class="card-text">  {{$curation->description }}</p>
                                    <p class="card-text">
                                        <small class="text-muted">Last
                                            updated {{$curation->updated_at->diffForHumans()}}</small>
                                    </p>
                                    <br>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>


            </div>
        @endif
    </div>
</div>


<div class="m-b-10"></div>

<!--end:: content -->

<!-- begin::Footer -->
@include('layouts/includes/footer')
<!-- end::Footer -->

<!-- begin::Modals -->
@include('layouts/includes/modals')
<!-- end::Modals -->


<!-- begin::Footer Scripts -->
<script src="{{ asset('/js/jquery.slim.min.js') }}"></script>
<script src="{{ asset('/js/jquery.min.js') }}"></script>

<!-- begin::Additional Scripts -->

<!-- end::Additional Scripts -->
<script src="{{ asset('/assets/plugins/bootstrap/413/js/bootstrap.min.js')  }}"></script>


<script src="{{ asset('assets/plugins/jscroll/js/jquery.jscroll.min.js')  }}"></script>
<script type="text/javascript">
    $('ul.pagination').hide();
    $(function () {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img class="center-block" src="/images/loading.gif" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function () {
                $('ul.pagination').remove();
            }
        });
    });
</script>


<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>


<script>
    $(".cb-checkbox").change(function () {

        $("label[for='" + $(this).attr("id") + "']").button('toggle');

        $("#test-if-checked").hide();
        if (this.checked) {
            $("#test-if-checked").show();

        }
    });

    $(document).on('touchstart', function () {
        $("label").addClass("no-hover");
    });

</script>

<script>
    $('.all_reset').on('change', function () {
        location.reload();
    });
</script>


<script>

    $(document).ready(function () {

        if (window.matchMedia("(min-width: 575px)").matches) {
            //alert("This is a mobile device.");
            $(".remove_on_575").remove();
        }

    });


</script>
<script>
    //Live Search
    $(document).ready(function () {
        $(".live-search").keyup(function () {

            // Retrieve the input field text and reset the count to zero
            var filter = $(this).val(), count = 0;

            // Loop through the comment list
            $(".masonry .item").each(function () {

                // If the list item does not contain the text phrase fade it out
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).fadeOut();

                    // Show the list item if the phrase matches and increase the count by 1
                } else {
                    $(this).show();
                    count++;
                }
            });

            // Update the count
            var numberItems = count;
            $("#filter-count").text("Number of Comments = " + count);
        });
    });
</script>


</body>
</html>




