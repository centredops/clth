@extends('layouts.front')
@section('title', 'Curation | Capsules')

@section('additional-css')
    <link rel="stylesheet" href="{{ asset('/assets/plugins/capsule-featured/style.css') }}">

@endSection


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 home-featured-column" onclick="location.href='/curation'">
                @if($homeCurations->toArray())
                    @foreach($homeCurations as $homeCuration)
                <div class="home-featured">
                    <img src="{{ asset('assets/images/curation-featured/' . $homeCuration->file) }}" alt="" width="100%" height="100%" />
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-t-10">&nbsp;</div>
                        <p>{{ $homeCuration->description }}</p>
                    </div>
                </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="m-t-50"></div>
                            <h3 class="no-content">Please load Home Curation Content</h3>
                            <div class="m-t-50"></div>
                        </div>
                    </div>
               @endif
               <a href="{{ route('front.curation') }}" class=" btn btn-outline-dark btn-rounded hvr-push">CURATION</a>
            </div>
            <div class="col-md-6 home-featured-column" onclick="location.href='/capsule'">
                   <div class="home-featured">
                    @include('layouts.includes.featured-capsule')
                    </div>
                @if($homeCapsules->toArray())
                    @foreach($homeCapsules as $homeCapsule)
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-t-10">&nbsp;</div>
                        <p>{{ $homeCapsule->description }}</p>
                    </div>
                </div>
                    @endforeach
                    @else
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="m-t-50"></div>
                            <h3 class="no-content">Please load Home Capsule Content</h3>
                            <div class="m-t-50"></div>
                        </div>
                    </div>
                @endif
                    <a href="{{ route('front.capsule') }}"class=" btn btn-outline-dark btn-rounded">CAPSULE</a>
             </div>
        </div>
    </div>

@endSection

@section('additional-scripts')
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-featured/wowslider.js')  }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-featured/script.js')  }}"></script>
@endSection