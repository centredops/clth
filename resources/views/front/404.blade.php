@extends('layouts.front')
@section('title', 'Not Founds')




@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="m-t-75"></div>
                <h3>Connecting Dots...</h3>
                <p class="text-center"><img src="{{ asset('/assets/images/404/404.png') }}" class="img-fluid block m-auto" width="400"/></p>
                <p class="text-center">Hello, we are probably still working on this, come <a href="/">back</a> Later.</p>
            </div>
        </div>
    </div>

@endSection

@section('additional-scripts')
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-featured/wowslider.js')  }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/plugins/capsule-featured/script.js')  }}"></script>
@endSection