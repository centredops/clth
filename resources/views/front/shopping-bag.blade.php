@extends('layouts.front')
@section('title', 'Shopping Bag | Capsules')

@section('additional-css')


@endSection


@section('content')
    <?php $total = 0 ?>
    <div class="container">
        @if(session('bag'))
        <div class="row">
            <div class="wrap cf" style="width:100%;">
                <h6 class="projTitle">YOUR BAG</h6>

                <div class="heading cf">
                    <a href="{{ route('front.checkout-bag') }}" class="continue">Checkout</a>
                    <a href="{{ route('front.capsule') }}" class="continue mr-3">Continue Shopping</a>


                </div>
                <div class="cart">
                    <ul class="cartWrap">

                            @foreach(session('bag') as $id => $details)
                                <?php $total += $details['price'] * $details['quantity'] ?>
                                <li class="items">

                                    <div class="infoWrap">
                                        <div class="cartSection">
                                            <img src="{{ asset('assets/images/capsules/'. $details['file']) }}"
                                                 width="113" alt="{{ $details['title'] }}" class="itemImg"/>
                                            <p class="itemNumber">{{ $details['code'] }}</p>
                                            <h3>{{ $details['title'] }}</h3>

                                            <p><input type="text" name="quality" class="qty"
                                                      placeholder="{{ $details['quantity'] }}"/> x
                                                R{{ $details['price'] }}</p>

                                            <p class="stockStatus"> In Stock</p>
                                        </div>


                                        <div class="prodTotal cartSection">
                                            <p>R{{ $details['price'] }}</p>
                                        </div>
                                        <div class="cartSection removeWrap">
                                            <a href="#" class="remove remove-shopping-bag" data-id="{{ $id }}">x</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach


                    </ul>
                </div>

                <div class="promoCode d-none"><label for="promo">Have A Promo Code?</label><input type="text" name="promo"
                                                                                           placholder="Enter Code"/>
                    <a href="#" class="btn"></a></div>

                <div class="subtotal cf">
                    <ul>
                        <li class="totalRow "><span class="label">Subtotal</span><span
                                    class="value">R{{ $total }}</span></li>
                          @if($total <= 450)
                        <li class="totalRow "><span class="label">Delivery Fee</span><span
                                    class="value">R50</span></li>
                        @else
                            <li class="totalRow "><span class="label">Delivery Fee</span><span
                                        class="value">Free</span></li>
                        @endif

                        <li class="totalRow" style="display: none"><span class="label">Shipping</span><span
                                    class="value">R0.00</span></li>

                        <li class="totalRow" style="display: none"><span class="label">VAT</span><span class="value">R110.00</span>
                        </li>
                        @if($total <= 450)
                        <li class="totalRow final"><span class="label">Total</span><span
                                    class="value">R{{ $total + 50}}</span></li>
                        @else
                            <li class="totalRow final"><span class="label">Total</span><span
                                        class="value">R{{ $total}}</span></li>
                        @endif
                        <li class="totalRow"><a href="{{ route('front.checkout-bag') }}" class="btn2 continue">Checkout</a></li>
                    </ul>
                </div>
            </div>
        </div>
            @else
            <h6 class="projTitle">YOUR BAG</h6>

            <div class="heading cf text-center">
                <h4>Your <strong>Bag</strong> is empty!</h4>
            </div>
        @endif
    </div>

@endSection

@section('additional-scripts')
    <script type="text/javascript">

        $(".update-shopping-bag").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            $.ajax({
                url: '{{ url('update-shopping-bag') }}',
                method: "patch",
                data: {
                    _token: '{{ csrf_token() }}',
                    id: ele.attr("data-id"),
                    quantity: ele.parents("tr").find(".quantity").val()
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        });

        $(".remove-shopping-bag").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            if (confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-shopping-bag') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

    </script>
@endSection