@extends('layouts.dashboard')
@section('title', 'Uploads')

@section('additional-css')
    <link href="dassets/libs/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="dassets/libs/datatables/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <link href="dassets/libs/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Home Page</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Manage Home Page</h4>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Upload</h4>
                                <p class="sub-header">
                                <hr />
                                </p>
                                <form method="post" action="{{url('uploads/store')}}" enctype="multipart/form-data"
                                      class="dropzone" id="dropzone">
                                    @csrf
                                </form>
                           </div>
                        </div>

                    </div>


                </div>


            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection


@section('additional-js')
    <script src="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>
    <script src="{{ asset('/dassets/js/pages/form-advanced.init.js') }}"></script>
    <script src="{{ asset('/dassets/libs/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('dassets/js/pages/form-editor.init.js') }}"></script>


    <script type="text/javascript">
        Dropzone.options.dropzone =
            {
                maxFilesize: 10,
                renameFile: function (file) {
                    var dt = new Date();
                    var time = dt.getTime();
                    return time + file.name;
                },
                acceptedFiles: ".jpeg,.jpg,.png,.gif",
                addRemoveLinks: true,
                timeout: 60000,
                success: function (file, response) {
                    console.log(response);
                },
                error: function (file, response) {
                    return false;
                }
            };
    </script>





@endSection


