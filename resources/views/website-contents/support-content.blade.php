@extends('layouts.dashboard')
@section('title', 'Support')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/summernote/summernote-bs4.css') }}" rel="stylesheet" />
@endSection()



@section('content')
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Support</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Manage Support</h4>
                </div>
            </div>
            <form action="{{ route('website-contents.support-create')  }}" method="post">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <textarea id="summernote-editor" name="content"></textarea>
                                <input type="hidden" name="page_id" value="6" >
                                <input type="hidden" name="page" value="support-content" >

                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <button type="submit" class="btn btn-primary btn-block">Add OR Update </button>
                    </div>
                </div>
            </form>


        </div>
        <div class="m-t-75">&nbsp;</div>
        <div class="container-fluid">


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            @if($content)
                                @foreach($content as $data)
                                    {!! $data->content !!}

                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>



@endSection

@section('additional-js')
    <script src="{{ asset('dassets/libs/summernote/summernote-bs4.min.js') }}"></script>

    <!-- Init js -->
    <script src="{{ asset('dassets/js/pages/form-editor.init.js') }}"></script>
@endSection()



