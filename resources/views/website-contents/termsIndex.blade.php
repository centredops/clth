@extends('layouts.dashboard')
@section('title', 'Manage Terms')

@section('additional-css')
    <link href="/dassets/libs/summernote/summernote-bs4.css" rel="stylesheet" />
@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Terms</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Manage Terms</h4>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div id="summernote-editor">
                                    <h6>You can add terms to display on the website here</h6>
                                    <ul>
                                        <li>
                                            Select a text to reveal the toolbar.
                                        </li>
                                        <li>
                                            Edit rich document on-the-fly, so elastic!
                                        </li>
                                    </ul>
                                    <p>
                                        End of simple area
                                    </p>
                                </div> <!-- end summernote-editor-->
                            </div>
                        </div> <!-- end card-->
                    </div> <!-- end col-->
                </div>

                <div class="row">


                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="header-title mb-3 mt-0">Current T&C's</h5>

                                <div class="accordion custom-accordionwitharrow" id="accordionExample">
                                    <div class="card mb-1 shadow-none border">

                                        <a href="" class="text-dark" data-toggle="collapse"
                                           data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="m-0 font-size-16">
                                                    Terms Of Use <i
                                                            class="uil uil-angle-down float-right accordion-arrow"></i>
                                                </h5>
                                            </div>
                                        </a>

                                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                             data-parent="#accordionExample">
                                            <div class="card-body text-muted">
                                                Anim pariatur cliche reprehenderit, enim eiusmod high life
                                                accusamus terry richardson ad squid. 3 wolf moon officia aute,
                                                non cupidatat skateboard dolor brunch. Food truck quinoa
                                                nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua
                                                put a bird on it squid single-origin coffee nulla assumenda
                                                shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                                wes anderson cred nesciunt sapiente ea proident. Ad vegan
                                                excepteur butcher vice lomo. Leggings occaecat craft beer
                                                farm-to-table, raw denim aesthetic synth nesciunt you probably
                                                haven't heard of them accusamus labore sustainable VHS.
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card mb-0 shadow-none border">

                                        <a href="" class="text-dark collapsed" data-toggle="collapse"
                                           data-target="#collapseThree" aria-expanded="false"
                                           aria-controls="collapseThree">
                                            <div class="card-header" id="headingThree">
                                                <h5 class="m-0 font-size-16">
                                                    Remove this T&C's<i
                                                            class="uil uil-angle-down float-right accordion-arrow"></i>
                                                </h5>
                                            </div>
                                        </a>

                                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                             data-parent="#accordionExample">
                                            <div class="card-body text-muted">
                                                <button class="btn btn-block btn--md btn-danger" type="submit">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->


    </div> <!-- content -->
@endSection

@section('additional-js')
    <script src="dassets/libs/summernote/summernote-bs4.min.js"></script>

    <!-- Init js -->
    <script src="dassets/js/pages/form-editor.init.js"></script>
@endSection()



