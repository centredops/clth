@extends('layouts.dashboard')
@section('title', 'Curation Categories')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/datatables/responsive.bootstrap4.min.css"') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Curation Categories</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Curation Categories</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')

                <div class="row">

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Categories</h4>
                                <p class="sub-header"></p>
                                <form action="{{ route('curation-categories.add-category') }}">
                                    {{ csrf_field() }}
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="name"  placeholder="Men | Women">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <textarea class="form-control" rows="5" name="description"></textarea>
                                    </div>
                                </div>
                                <button class="btn btn-block btn--md btn-primary" type="submit">Submit</button>
                                </form>

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">

                                <div class="table-responsive">
                                    <table class="table mb-0" id="category_crud">
                                        <thead>
                                        <tr>
                                            <th scope="col">Type</th>
                                            <th scope="col"></th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="posts-crud">
                                        @if(!empty($categories->toArray()))

                                            @foreach($categories as $category)
                                         <tr id="category_id_{{ $category->id }}">
                                            <td>{{ $category->name }}</td>
                                            <td> <a href="javascript:void(0)" class="btn btn-info btn-block" id="edit-category" data-id="{{ $category->id }}">Edit</a></td>
                                            <td> <a href="{{ route('curation-categories.delete-category', $category->id ) }}" class="btn btn-danger btn-block"> Delete </a></td>
                                        </tr>
                                        @endforeach
                                       @else
                                        <tr>
                                            <td colspan="2" class="text-center p-tb-15">There are currently no categories loaded.</td>
                                        </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>

                        <!--  begin:: Modals -->
                    @include('curation-categories.modals')
                    <!--  end:: Modals -->

                    </div>
                </div>

            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection

@section('additional-js')
    <script src="{{ asset('/dassets/js/jquery.validate.js') }}"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('body').on('click', '#edit-category', function () {
                var category_id = $(this).data('id');
                $.get('/edit-category/'+category_id, function (data) {
                    $('#postCrudModal').html("Update Category");
                    $('#btn-save').val("edit-category");
                    $('#ajax-crud-modal').modal('show');
                    $('#category_id').val(data.id);
                    $('#name').val(data.name);
                    $('#description').val(data.description);
                })
            });

        });

        if ($("#categoryForm").length > 0) {
            $("#categoryForm").validate({

                submitHandler: function(form) {

                    var actionType = $('#btn-save').val();

                    $('#btn-save').html('Updated....');


                    $.ajax({
                        data: $('#categoryForm').serialize(),
                        url: "{{ route('curation-categories.store-category') }}",
                        type: "GET",
                        dataType: 'json',
                        success: function (data) {

                            //$('#postForm').trigger("reset");
                            $('#postForm').hide();
                            $('#ajax-crud-modal').modal('hide');
                            $('#btn-save').html('Save Changes');
                            location.reload();

                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $('#btn-save').html('Save Changes');
                        }
                    });

                }
            })

        }
    </script>


@endSection



