

<!--  Modal -->

<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="postCrudModal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="categoryForm" name="categoryForm" class="form-horizontal">
                    <input type="hidden" name="category_id" id="category_id">
                    <div class="form-group row">

                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Category Name" >
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <textarea class="form-control" rows="5" id="description" name="description" placeholder="Description"></textarea>
                        </div>
                    </div>
               
                    <div class="form-group row">

                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-primary btn-block" id="btn-save" value="create">Update Category</button>
                        </div>
                        <div class="col-sm-6">
                            <button  class="btn btn-info btn-block" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>







