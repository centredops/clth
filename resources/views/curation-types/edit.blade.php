@extends('layouts.dashboard')
@section('title', 'Curation Types')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Curation Types</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Curation Types</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')

                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Types</h4>
                                <p class="sub-header"></p>
                                <form action="{{ route('curation-types.update', $type->id) }}" method="post">
                                    {{ csrf_field() }}

                                    {{ method_field('PATCH') }}

                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="name" value="{{ $type->name }}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <a href="#" class="btn btn-block btn--md btn-danger" onclick="goBack()">Cancel</a>
                                        </div>
                                        <div class="col-lg-6">
                                            <button class="btn btn-block btn--md btn-primary" type="submit">Update Type</button>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection

@section('additional-js')




@endSection



