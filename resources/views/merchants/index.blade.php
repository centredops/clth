@extends('layouts.dashboard')
@section('title', 'Merchants')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"  type="text/css" />
    <link href="{{ asset('/dassets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('dassets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css" rel="stylesheet" />
@endSection



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Colors</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Manage Merchants</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')
                <div class="row">

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Merchants</h4>
                                <p class="sub-header"></p>
                                <form action="{{ route('merchants.store') }}" method="post"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="form-group row">
                                        <label class="col-lg-12">Merchant Logo</label>
                                        <div class="col-lg-12">
                                            <input type="file" name="file" class="form-control"/>
                                        </div>
                                    </div>

                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" value="{{ old('name') }}" name="name" placeholder="Merchant">
                                    </div>
                                </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" value="{{ old('description') }}" name="description" placeholder="Description">
                                        </div>
                                    </div>


                                <button class="btn btn-block btn--md btn-primary" type="submit">Add Merchant</button>

                                    <button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#upload">Upload Featured Merchant Sliders</button>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                {{--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#ajax-crud-modal">Large modal</button>--}}
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                        <tr>
                                            <th scope="col">Merchant</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="posts-crud">
                                        @if(!empty($merchants->toArray()))
                                            @foreach( $merchants as $merchant)
                                                <tr>
                                                    <td><img src="{{ asset('assets/images/merchants/' . $merchant->file) }}" alt="" class="img-fluid" width="80"/></td>
                                                    <td>{{ $merchant->name }}</td>
                                                    <td>
                                                        <form action="{{url('merchants', [$merchant->id])}}" method="POST">
                                                            {{method_field('DELETE')}}
                                                            @csrf
                                                            <input type="submit" class="btn btn-danger btn-block" value="Delete Merchant"/>
                                                        </form>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="text-center p-tb-15">There are no merchants loaded yet.</td>
                                            </tr>
                                        @endif

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


                <div class="row">
                    @if($images->toArray())
                        @foreach($images as $image)
                            <div class="card mb-3 mb-xl-0">
                                <img class="card-img-top img-fluid" src="{{ asset('assets/images/capsule-featured-list/' . $image->file ) }}" alt="" style="max-width: 400px;">
                                <div class="card-body">
                                    <form action="{{ route('merchants.delete-image',$image->id) }}" method="POST">
                                        @csrf
                                        <input type="submit" class="btn btn-danger btn-block" value="Delete"/>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>




            </div>
        </div>
    </div>

    <!--  begin:: Modals -->
    @include('merchants.modals')
    <!--  end:: Modals -->

@endSection





@section('additional-js')
    <script src="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>
    <script src="{{ asset('/dassets/js/pages/form-advanced.init.js') }}"></script>
    <script src="{{ asset('/dassets/libs/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('dassets/js/pages/form-editor.init.js') }}"></script>







@endSection






