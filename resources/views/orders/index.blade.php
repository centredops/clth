@extends('layouts.dashboard')
@section('title', 'Order List')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->

            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Order List</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Order List</h4>
                    </div>
                </div>

                @include('.orders.order-list')


            </div>


        </div> <!-- content -->
    @endSection


    @section('additional-js')

        <!-- datatable js -->
            <script src="{{ asset('/dassets/libs/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('dassets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/dataTables.responsive.min.js') }}"></script>
            <script src="{{ asset('/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

            <script src="{{ asset('/dassets/libs/datatables/dataTables.buttons.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.html5.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.flash.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.print.min.js') }}"></script>

            <script src="{{ asset('/dassets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/dataTables.select.min.js') }}"></script>



            <!-- Datatables init -->
            <script src="{{ asset('/dassets/js/pages/datatables.init.js') }}"></script>
            <script src="{{ asset('dassets/js/pages/form-advanced.init.js') }}"></script>

@endSection
