

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                @if($orders->isEmpty())
                    <h4 class="text-center">There are currently no <strong>Orders</strong> in the system</h4>
                    @else
                    <table id="basic-datatable" class="table dt-responsive nowrap">
                        <thead>
                        <tr>
                            <th>Order Number</th>
                            <th>Date Created</th>
                            <th>Order Status</th>
                            <th>Manage</th>
                        </tr>
                        </thead>


                        <tbody>

                        @foreach($orders as $order)

                            <tr>
                                <td>{{ $order->order_number }}</td>
                                <td>{{ $order->created_at }}</td>
                                <td>
                                    @if($order->status == 'Pending')
                                        <span class="badge badge-soft-warning py-1">{{ $order->status }}</span>
                                    @elseif ($order->status == 'Cancelled')
                                        <span class="badge badge-soft-danger py-1">{{ $order->status }}</span>
                                    @elseif ($order->status == 'Approved')
                                        <span class="badge badge-soft-success py-1">{{ $order->status }}</span>
                                        @else
                                        <span class="badge badge-soft-warning py-1">{{ $order->status }}</span>
                                    @endif
                                </td>
                                <td class="text-center"> <a href="{{ route('orders.show', $order->order_number ) }}" class="btn btn-outline-primary btn-block btn-group-lg">Manage Order</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>