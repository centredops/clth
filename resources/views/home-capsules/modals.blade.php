<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">Upload</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="{{ route('home-capsules.update', 1) }}" name="file" method="post" class="dropzone" id="fileUploads" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    {{ method_field('PATCH') }}

                    <input type="hidden" name="file_id" value="1">
                    <input type="hidden" name="type" value="home_capsule">
                    <div class="fallback">
                        <input name="file" type="file"  id="file" multiple />
                    </div>

                    <div class="dz-message needsclick">
                        <i class="h1 text-muted  uil-cloud-upload"></i>
                        <h3>Drop files here or click to upload.</h3>
                        <span class="text-muted font-13">(Please drop all <strong>Capsule</strong> images to display on home page.</span>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>