@extends('layouts.dashboard')
@section('title', 'Capsule Sizes')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Capsule Sizes</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Capsule Sizes</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')

                <div class="row">

                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Sizes</h4>
                                <p class="sub-header"></p>
                                <form action="{{ route('capsule-sizes.store') }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="name" placeholder="i.e Small">
                                        </div>
                                    </div>

                                    <button class="btn btn-block btn--md btn-primary" type="submit">Submit</button>
                                </form>

                            </div>
                        </div>

                    </div>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body">
                                <table id="basic-datatable" class="table dt-responsive nowrap">
                                    <thead>
                                    <tr>
                                        <th scope="col">Size</th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody id="posts-crud">
                                    @if(!empty($sizes->toArray()))

                                        @foreach($sizes as $size)
                                            <tr>
                                                <td>{{ $size->name }}</td>
                                                <td><a href="{{ route('capsule-sizes.edit', $size->id) }}" class="btn btn-info btn-block">Edit</a></td>
                                                <td>
                                                    <form action="{{url('capsule-sizes', [$size->id])}}" method="POST">
                                                        {{method_field('DELETE')}}
                                                        @csrf
                                                        <input type="submit" class="btn btn-danger btn-block" value="Delete"/>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="2" class="text-center p-tb-15">There are currently no sizes
                                                loaded.
                                            </td>
                                        </tr>
                                    @endif

                                    </tbody>
                                </table>

                            </div>
                        </div>

                        <!--  begin:: Modals -->
                   
                    <!--  end:: Modals -->

                    </div>
                </div>

            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection

@section('additional-js')
    <script src="{{ asset('/dassets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dassets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('/dassets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('/dassets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/dataTables.select.min.js') }}"></script>



    <!-- Datatables init -->
    <script src="{{ asset('/dassets/js/pages/datatables.init.js') }}"></script>
    <script src="{{ asset('dassets/js/pages/form-advanced.init.js') }}"></script>




@endSection



