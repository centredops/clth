@extends('layouts.dashboard')
@section('title', 'User Profile')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />

@endSection()


@section('content')
    <div class="content">

        <!-- Start Content-->
        <div class="container-fluid">
            <div class="row page-title">
                <div class="col-md-12">
                    <nav aria-label="breadcrumb" class="float-right mt-1">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Pages</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Profile</li>
                        </ol>
                    </nav>
                    <h4 class="mb-1 mt-0">Profile</h4>
                </div>
            </div>

            @include('layouts.includes.success')
            @include('layouts.includes.errors')

            <div class="row">
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mt-3">

                                @if(Auth::user()->file)
                                    <img src="{{ asset('assets/images/users/' . Auth::user()->file) }}" alt="" class="avatar-lg rounded-circle"/>

                                    @else

                                    <img src="{{ asset('assets/images/users/default.jpg') }}" alt="" class="avatar-lg rounded-circle"/>
                                 @endif


                                <h5 class="mt-2 mb-0">{{ Auth::user()->name }}</h5>
                                <h6 class="text-muted font-weight-normal mt-2 mb-0">Role
                                </h6>
                                <h6 class="text-muted font-weight-normal mt-1 mb-4">[ {{ Auth::user()->role->name }}
                                    ]</h6>

                            </div>

                            <div class="mt-3 pt-2 border-top">
                                <h4 class="mb-3 font-size-15">Contact Information</h4>
                                <div class="table-responsive">
                                    <table class="table table-borderless mb-0 text-muted">
                                        <tbody>
                                        <tr>
                                            <th scope="row">Email</th>
                                            <td>{{ Auth::user()->email }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Cell</th>
                                            <td>{{ Auth::user()->cell }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->

                </div>

                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-activity-tab" data-toggle="pill"
                                       href="#pills-activity" role="tab" aria-controls="pills-activity"
                                       aria-selected="true">
                                        Admin Profile
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="pills-files-tab" data-toggle="pill"
                                       href="#pills-files" role="tab" aria-controls="pills-files"
                                       aria-selected="false">
                                        Manage Users
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-activity" role="tabpanel"
                                     aria-labelledby="pills-activity-tab">
                                    <div class="row">

                                        <div class="col-lg-12">
                                            <div class="card">
                                                <form action="{{ route('users.update-my-profile', Auth::user()->id) }}" method="POST" enctype="multipart/form-data">

                                                    {{ csrf_field() }}


                                                <div class="card-body">
                                                    <div class="form-group row">
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" name="name"
                                                                   value="{{ Auth::user()->name }}">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" name="surname"
                                                                   value="{{ Auth::user()->surname }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="cell"
                                                                   value="{{ Auth::user()->cell }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="email"
                                                                   value="{{ Auth::user()->email }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-lg-12">
                                                            <input type="text" class="form-control" name="password"
                                                                   placeholder="Enter New Password">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label>Upload Profile Image</label>
                                                            <input type="file" name="file" class="form-control"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <button class="btn btn-block btn--md btn-primary" type="submit">Update My
                                                                Profile
                                                            </button>
                                                        </div>
                                                    </div>


                                                </div>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                  </div>


                                <div class="tab-pane fade" id="pills-files" role="tabpanel"
                                     aria-labelledby="pills-files-tab">
                                   <div class="row">
                                       <div class="col-md-12">
                                           @if($users->toArray())
                                          <table id="basic-datatable" class="table dt-responsive nowrap">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Surname</th>
                                                <th>Email</th>
                                                <th>Cell</th>
                                                <th>Role</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>


                                            <tbody>
                                                @foreach($users as $user)
                                                <tr>
                                                    <td>{{$user->name }}</td>
                                                    <td>{{$user->surname }}</td>
                                                    <td>{{$user->email }}</td>
                                                    <td>{{$user->cell }}</td>
                                                    <td>{{ $user->role->name }}</td>
                                                    <td>
                                                        @if($user->is_active == 1)
                                                            <span class="badge badge-soft-success py-1">Active</span>
                                                        @else
                                                            <span class="badge badge-soft-danger py-1">Not Active</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{ route('users.edit-user', [$user->id]) }}" class="btn btn-success btn-block">Edit</a>
                                                    </td>
                                                    <td class="text-center"> <a href="{{ route('users.delete-user', [$user->id]) }}" class="btn btn-danger btn-block">Delete</a></td>
                                                    <td>

                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                           @endif
                                       </div>
                                   </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- end card -->
                </div>
            </div>
            <!-- end row -->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection

@section('additional-js')


    <!-- datatable js -->
    <script src="{{ asset('/dassets/libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dassets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('/dassets/libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/buttons.print.min.js') }}"></script>

    <script src="{{ asset('/dassets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/datatables/dataTables.select.min.js') }}"></script>

@endSection



