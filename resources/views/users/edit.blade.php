@extends('layouts.dashboard')
@section('title', 'Edit User')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/datatables/responsive.bootstrap4.min.css"') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Edit {{ $showUser->name }} Details</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')

                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Update</h4>
                                <p class="sub-header"></p>
                                <form action="{{ route('users.update-user', $showUser->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label>Role</label>
                                            <select name="role_id" class="form-control">
                                                <option value="" selected disabled hidden>Select Role</option>
                                                <option value="1">Administrator</option>
                                                <option value="2">Subscriber</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Status</label>
                                            <select name="is_active" class="form-control">
                                                <option value="" selected disabled hidden>Status</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" name="name"  value="{{ $showUser->name }}">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="name">Surname</label>
                                            <input type="text" class="form-control" name="surname"  value="{{ $showUser->surname }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label for="name">Cell</label>
                                            <input type="text" class="form-control" name="cell"  value="{{ $showUser->cell }}">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="name">Email</label>
                                            <input type="text" class="form-control" name="email"  value="{{ $showUser->email }}">
                                        </div>
                                    </div>
                                    <button class="btn btn-block btn--md btn-primary" type="submit">Update User</button>
                                </form>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- end row-->
        </div> <!-- container-fluid -->

    </div> <!-- content -->
@endSection

@section('additional-js')


@endSection



