@extends('layouts.dashboard')
@section('title', 'Home Curation')

@section('additional-css')

    <link href="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/select2/select2.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/multiselect/multi-select.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet"/>

    <link href="{{ asset('/dassets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/summernote/summernote-bs4.css') }}" rel="stylesheet"/>

@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Home Curation</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')


                <div class="row">

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{ route('home-curations.store') }}" method="post"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label class="col-lg-12 col-form-label" for="description">Description</label>
                                        <div class="col-lg-12">
                                            <textarea class="form-control" rows="5" name="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label>Upload Home Curation Image <span style="color:red;">(1200 X 800)</span></label>
                                            <input type="file" name="file" class="form-control"/>
                                        </div>
                                    </div>

                                    <button class="btn btn-block btn--md btn-primary" type="submit">Add Curation
                                    </button>
                                </form>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-body">
                                @if($details->toArray())

                                    @foreach($details as $detail)
                                        <div class="col-lg-12 col-xl-12">
                                            <p>{{ $detail->description }}</p>
                                            <hr/>
                                        </div>
                                        <div class="col-lg-12 col-xl-12">
                                            <p><img class="card-img-top img-fluid"
                                                    src="{{ asset('/assets/images/curation-featured/' . $detail->file ) }}"
                                                    alt="Banner 1"></p>
                                            <form action="{{url('home-curations', [$detail->id])}}" method="POST">
                                                {{method_field('DELETE')}}
                                                @csrf
                                                <input type="submit" class="btn btn-danger btn-block" value="Delete Content"/>
                                            </form>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="col-lg-12 col-xl-12">
                                        <p class="text-center">Please add <strong>Curation Featured</strong> for home page.</p>
                                        <hr/>
                                    </div>

                                @endif


                            </div>

                        </div>


                    </div>


                </div>

            </div> <!-- content -->

            @endSection


            @section('additional-js')
                <script src="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
                <script src="{{ asset('/dassets/libs/select2/select2.min.js') }}"></script>
                <script src="{{ asset('/dassets/libs/multiselect/jquery.multi-select.js') }}"></script>
                <script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>
                <script src="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
                <script src="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
                <script src="{{ asset('/dassets/libs/dropzone/dropzone.min.js') }}"></script>
                <script src="{{ asset('/dassets/js/pages/form-advanced.init.js') }}"></script>
                <script src="{{ asset('/dassets/libs/summernote/summernote-bs4.min.js') }}"></script>
                <script src="{{ asset('dassets/js/pages/form-editor.init.js') }}"></script>


@endSection
