

<!--  Modal -->

    <div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="postCrudModal"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="colorForm" name="colorForm" class="form-horizontal">
                    <input type="hidden" name="color_id" id="color_id">
                    <div class="form-group row">

                        <div class="col-lg-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Color Name" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <input type="text" id="hex" class="form-control" name="hex" value="#4a81d4" placeholder="Color Picker" >
                        </div>
                    </div>
                    <div class="form-group row">

                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-primary btn-block" id="btn-save" value="create">Update Color</button>
                        </div>
                        <div class="col-sm-6">
                            <button  class="btn btn-info btn-block" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </form>
             </div>
        </div>
    </div>
</div>







