@extends('layouts.dashboard')
@section('title', 'Curation Colors')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"  type="text/css" />
    <link href="{{ asset('/dassets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('dassets/libs/multiselect/multi-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet" type="text/css"/>
@endSection



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Colors</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Manage Colors</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="header-title mt-0 mb-1">Colours</h4>
                                <p class="sub-header"></p>
                                <form action="{{ route('colors.update', $color->id) }}" method="post">
                                    {{ csrf_field() }}

                                    {{ method_field('PATCH') }}

                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" value="{{ $color->name }}" name="name" placeholder="Give color a name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-12">Select Color</label>
                                        <div class="col-lg-12">
                                            <input type="text" id="hex" class="form-control" name="hex" placeholder="Color Picker" value="{{ $color->hex }}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <a href="#" class="btn btn-block btn--md btn-danger" onclick="goBack()">Cancel</a>
                                        </div>
                                        <div class="col-lg-6">
                                            <button class="btn btn-block btn--md btn-primary" type="submit">Update Color</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>

                <!--  begin:: Modals -->
            @include('colors.modals')
            <!--  end:: Modals -->

            </div>
        </div>
    </div>


@endSection






@section('additional-js')
    <script src="{{ asset('/dassets/js/jquery.validate.js') }}"></script>
    <!-- Plugins Js -->
    <script src="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>


    <!-- Init js-->
    <script src="{{ asset('/dassets/js/pages/form-advanced.init.js') }}"></script>


@endSection()



