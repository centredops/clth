@if(session()->get('success'))
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success animated bounceInDown">
              {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
@endif




