<!-- begin::Sign Up -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Clth Sign-up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="social">
                    <li>
                        <a href="{{ route('login.twitter') }}" title="">
                            <span class="icon fab fa-twitter mytwitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('login.facebook') }}" title="">
                            <span class="icon fab fa-facebook-f"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('login.google') }}" >
                            <span class="icon fab fa-google-plus-g"></span>
                        </a>
                    </li>
                </ul>

                <div class="alternative text-center"><h4>OR</h4></div>

                <div class="col-md-10 text-center form-group">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                             <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  autocomplete="name"  placeholder="Name">

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="surname" type="text" class="form-control"  name="surname" value="{{ old('surname') }}"  autocomplete="surname" autofocus placeholder="surname">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="cell" type="text" class="form-control" name="cell" value="{{ old('cell') }}"  placeholder="Cell">
                            </div>
                        </div>

                        <div class="form-group row">


                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="Email Address">

                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password"  autocomplete="new-password" placeholder="Password">

                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-dark btn-block">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="modal-footer">
                <p>Already Have An Account? <a data-dismiss="modal" data-toggle="modal" data-target="#glassAnimals"
                                               href="#">Login</a></p>
            </div>
        </div>
    </div>
</div>
<!-- end::Sign Up -->

<!-- begin::Login -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-width" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Clth Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="loginsocial text-center"><h4>Login With:</h4></div>

                <ul class="social">
                    <li>
                        <a href="{{ route('login.twitter') }}">
                            <span class="icon fab fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('login.facebook') }}" >
                            <span class="icon fab fa-facebook-f"></span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('login.google') }}" >
                            <span class="icon fab fa-google-plus-g"></span>
                        </a>
                    </li>
                </ul>

                <div class="alternative text-center"><h4>OR</h4></div>
                <form method="POST" action="{{ route('login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group row">


                        <div class="col-md-12">
                            <input id="email" type="text" class="form-control"
                                   name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="Mobile Or E-mail">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">


                        <div class="col-md-12">
                            <input id="password" type="password" class="form-control" name="password"  autocomplete="current-password" placeholder="Password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>




                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-dark myloginbtn">
                                {{ __('Login') }}
                            </button>
                        </div>
                    </div>
                    <div class="form-group row visibility-hidden">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : 'checked' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="modal-footer">
                    <p style="font-size:11px;">Don't have an account? <a data-dismiss="modal" data-toggle="modal" data-target="#signupModal" href="">Sign-up</a>
                        |  <a data-dismiss="modal" data-toggle="modal" data-target="#forgotPasswordModal" href="">Forgot Password</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end::Login -->


<!-- begin::Login -->
<div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-width" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Clth Forgot Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group row">


                        <div class="col-md-12">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address">

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-lg btn-dark btn-block">
                                {{ __('Send Password Reset Link') }}
                            </button>
                        </div>
                    </div>
                </form>

</div>

                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end::Login -->



