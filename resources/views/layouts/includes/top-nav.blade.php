<div class="container-fluid display_desktop mainNav sticky-top">
    <div class="row">
        <div class="col-5">
            <div class="pull-left">
                <ul>
                    <div class="m-t-15"></div>
                    <li class=""><a href="{{ route('front.curation') }}" class="nav-link">Curation</a></li>
                    <li><a href="{{ route('front.capsule') }}" class="nav-link">Capsules</a></li>
                </ul>
            </div>
        </div>
        <div class="col-2 text-center">
            <a href="/" id="navlogo" class="hvr-push"><span>Clth</span></a>
        </div>
        <div class="col-5">
            <div class="pull-right">
                @guest
                    <ul>

                        <li><a data-toggle="modal" data-target="#loginModal" href="#" class="nav-link">Login</a></li>
                        <li><a data-toggle="modal" data-target="#signupModal" href="#" class="nav-link">Signup</a></li>
                        <li><a data-toggle="modal" data-target="#loginModal" href="#" class="nav-link">Coset</a></li>
                    </ul>
                @else
                    <ul>

                        @if (\Request::is('curation'))
                            <li>
                                <input type="search" class="live-search">
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('front.shopping-bag') }}" class="nav-link">Shopping Bag&nbsp;<span
                                        class="badge">{{ count((array) session('bag')) }}</span></a>
                        </li>
                        <li><a href="{{ route('front.shopping-closet') }}" class="nav-link">Closet</a></li>
                            <li>
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </li>

                    </ul>
                    @endif
            </div>
        </div>

    </div>
</div>


<div class="container-fluid display_mobile mainNav sticky-top">
    <div class="mobile-search animated slideInRight">
        <div class="input-group">
            <input type="text" class="form-control live-search" placeholder="Search Curations..">
            <div class="input-group-append">
                <button class="btn btn-primary hvr-push" type="button" id="hide_mobile_search">Cancel</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            <div class="pull-left">
                <table style="margin-top:7px;">
                    <tr>
                        <td>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle"
                                    aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
                                <img src="{{ asset('assets/images/nav-icon.png') }}" width="25" height="25" class="hvr-push navBtn animated bounceIn"/>
                            </button>



                        </td>
                        <td>
                            @if (\Request::is('curation'))
                                <img src="{{ asset('assets/images/mobile-search.png') }}" id="show_mobile_search" class="hvr-push" width="25" height="25"/>
                              @endif
                        </td>
                    </tr>
                </table>

                <div class="collapse navbar-collapse animated bounceInLeft" id="navbarToggle">
                    <div class="m-t-15"></div>
                    <ul class="navbar-nav p-l-10">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('front.curation') }}">Curation<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('front.capsule') }}">Capsules</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-2 text-center">
            <a href="/" id="navlogo" class="hvr-push"><span>Clth</span></a>
        </div>
        <div class="col-5">
            <div class="pull-right">
                <div class="m-t-10"></div>
                @guest
                    <ul>
                     <li><a data-toggle="modal" data-target="#loginModal" href="#" class="nav-link">Login</a></li>
                        <li><a data-toggle="modal" data-target="#signupModal" href="#" class="nav-link">Signup</a></li>
                    </ul>
                @else
                    <ul class="navbar">
                        <li>
                            <a class="hvr-push" href="{{ route('front.shopping-bag') }}"> <img src="{{ asset('assets/images/bag.png') }}" class="bag-menu ">
                                <span class="badge badge-pill badge-danger shopping-count">{{ count((array) session('bag')) }}</span></a>
                        </li>

                        <li>
                            <a class="hvr-push bag-cart dropdown-toggle" role="button" id="navbarDropdown"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                               href="#"><img src="{{ asset('assets/images/person.png') }}" class="closet-menu"></a>
                            <div class="dropdown-menu dropdown-menu-right animate slideIn" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('front.shopping-closet') }}">Closet</a>
                                {{--<a class="dropdown-item" href="#">Another action</a>--}}
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </div>
                        </li>

                    </ul>
                @endif
            </div>
        </div>

    </div>
</div>
