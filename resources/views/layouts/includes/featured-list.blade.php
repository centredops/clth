
@if($featuredMerchants->toArray())

    <div id="wowslider-container1">
        <div class="ws_images">
            <ul>
            @foreach($featuredMerchants as $featuredMerchant)
                    <li><img src="{{ asset('assets/images/capsule-featured-list/' . $featuredMerchant->file ) }}" alt="{{ $featuredMerchant->type }}" title="{{ $featuredMerchant->type }}" /></li>
                @endforeach
            </ul>
        </div>
        <div class="ws_shadow"></div>
    </div>
@else
    <h3 class="no-content text-center">Load Slider Images</h3>
@endif