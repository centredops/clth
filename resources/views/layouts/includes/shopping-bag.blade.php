<div class="cart">
    <ul class="cartWrap">
        <?php $total = 0 ?>

        @if(session('bag'))
            @foreach(session('bag') as $id => $details)
                <?php $total += $details['price'] * $details['quantity'] ?>
                <li class="items">

                    <div class="infoWrap">
                        <div class="cartSection">
                            <img src="{{ asset('assets/images/capsules/'. $details['file']) }}"
                                 width="113" alt="{{ $details['title'] }}" class="itemImg"/>
                            <p class="itemNumber">{{ $details['code'] }}</p>
                            <h3>{{ $details['title'] }}</h3>

                            <p><input type="text" name="quality" class="qty"
                                      placeholder="{{ $details['quantity'] }}"/> x
                                R{{ $details['price'] }}</p>

                            <p class="stockStatus d-none"> In Stock</p>
                        </div>


                        <div class="prodTotal cartSection">
                            <p>R{{ $details['price'] }}</p>
                        </div>
                        <div class="cartSection removeWrap">
                            <a href="#" class="remove remove-shopping-bag" data-id="{{ $id }}">x</a>
                        </div>
                    </div>
                </li>
            @endforeach
        @endif

    </ul>
</div>

<div class="promoCode"><label for="promo">Have A Promo Code?</label><input type="text" name="promo"
                                                                           placholder="Enter Code"/>
    <a href="#" class="btn"></a></div>

<div class="subtotal cf">
    <ul>
        <li class="totalRow "><span class="label">Subtotal</span><span
                    class="value">R{{ $total }}</span></li>

        <li class="totalRow" style="display: none"><span class="label">Shipping</span><span
                    class="value">R0.00</span></li>

        <li class="totalRow" style="display: none"><span class="label">VAT</span><span class="value">R110.00</span>
        </li>
        <li class="totalRow final"><span class="label">Total</span><span
                    class="value">R{{ $total }}</span></li>
        <li class="totalRow"><a href="{{ route('front.checkout-bag') }}" class="btn2 continue">Checkout</a></li>
    </ul>
</div>