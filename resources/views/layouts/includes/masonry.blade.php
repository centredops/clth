<div id="masonry" class="grid infinite-scroll">

    @if($curations)
        @foreach($curations as $curation)


            <div class="animated fadeIn card-container contsearch simple  {{$curation->category->name }} {{$curation->color->name }} {{$curation->type->name }}"
                 onclick="location.href='/curation-post/{{ $curation->id }}'">

                <div class="card gsearch">
                    <img src="{{ asset('/assets/images/curation/' . $curation->file) }}" class="img-fluid"
                         align="{{ $curation->title }}"/>
                    <h4>{{$curation->title }}</h4>
                    <p>
                        {{$curation->description }}
                    </p>
                    <span class="time">Last updated {{$curation->updated_at->diffForHumans()}}</span>
                </div>
            </div>
            <div id="localSearchSimple"></div>


        @endforeach
        {{--{{ $curations->links() }}--}}
    @else
        <div class="m-t-75"></div>
        <h3 class="text-center">Watch this space, there are currently no <strong>Curations</strong> listed</h3>
    @endif
</div>
</div>



