<!-- Start WOWSlider.com BODY section --> <!-- add to the <body> of your page -->
@if($homeSliders->toArray())

<div id="wowslider-container1">
    <div class="ws_images">
            <ul>
                @foreach($homeSliders as $homeSlider)
            <li><a href="/capsule"> <img src="{{ asset('assets/images/capsule-featured/' . $homeSlider->file ) }}" alt="{{ $homeSlider->type }}" title="{{ $homeSlider->type }}" /></a></li>
          @endforeach
        </ul>
    </div>
    <div class="ws_shadow"></div>
</div>
    @else
    <h3 class="no-content text-center">Load Slider Images</h3>
@endif
