<footer class="footer" >
    <div class="container bottom_border">
        <div class="col-12">
            <ul class="foote_bottom_ul_amrc text-center col-md-12 col-lg-12">
                <li><a href="{{ route('front.about-us') }}">Our Story</a></li>
                <li><a href="{{ route('front.business') }}">Business</a></li>
                <li><a href="{{ route('front.service-terms') }}">Terms Of Service</a></li>
                <li><a href="{{ route('front.privacy-policy') }}">Privacy Policy</a></li>
                <li><a href="{{ route('front.support') }}">Support</a></li>
                <li><a href="{{ route('front.contact-us') }}">Contact</a></li>

            </ul>
        </div>
        <div class="col-12">
            <!-- begin:: Socials -->
            <div class="text-center"><span><ul class="list-inline social-links"><li><a href=""><i class="fab fa-facebook-f"></i></a></li><li><a href=""><i class="fab fa-twitter"></i></a></li></li><li><a href=""><i class="fab fa-instagram"></i></a></li></ul></span></div>
            <!-- end:: Socials -->
        </div>
        <div class="col-12">
            <!-- begin:: Copyright -->
            <p class="text-center">Copyright <?php echo date('Y'); ?>: <a href="" class="footerdomain">Clth: Curation & Capsules</a> </p>
            <!-- end:: Copyright -->
        </div>
    </div>
</footer>

@if(\Request::is('shopping-closet'))
<div class="socialFooterContainer"><a href="https://wa.me//27672002107/?text=Good day, I need support"></a></div>
@endif


