<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title') - CLTH </title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon/favicon.ico') }}">
    <meta name="robots" content="all,follow">
    <link href="{{ asset('/css/defaults.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/plugins/aos/aos.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/assets/plugins/animate/animate.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css/defaults.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/bootstrap/413/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/font-awsome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/styles.css') }}">

@yield('additional-css')
<!-- end::Additional Stylesheet -->

    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
    <script src="{{ asset('js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/respond.min.js') }}"></script>
    <![endif]-->
</head>
<body>

<!--begin: top nav -->

<div class="m-t-10"></div>
@include('layouts/includes/top-nav')
<div class="m-t-40"></div>
<!--end: top nav -->

<!--begin:: content -->
<div class="container-fluid">
    @include('layouts.includes.success')
    @include('layouts.includes.errors')
</div>
@yield('content')

<div class="m-b-10"></div>

<!--end:: content -->

<!-- begin::Footer -->
@include('layouts/includes/footer')
<!-- end::Footer -->

<!-- begin::Modals -->
@include('layouts/includes/modals')
<!-- end::Modals -->

<!-- begin::Footer Scripts -->
<script src="{{ asset('/js/jquery.slim.min.js') }}"></script>
<script src="{{ asset('/js/jquery.min.js') }}"></script>

<script src="{{ asset('/assets/plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>

<!-- begin::Additional Scripts -->

<!-- end::Additional Scripts -->
<script src="{{ asset('/assets/plugins/bootstrap/413/js/bootstrap.min.js')  }}"></script>
<script src="{{ asset('/assets/plugins/descrambler/descrambler.min.js') }}"></script>
<script src="{{ asset('/assets/plugins/aos/aos.js') }}"></script>
@yield('additional-scripts')
<script type="text/javascript" src="/js/custom.js"></script>
<script type="text/javascript" src="/js/scripts.js"></script>
<script>

    $.LoadingOverlaySetup({
        background              : "rgba(255, 255, 255, 0.9)",
        image                   : "",
        progress                : true,
        text                    : "CLTH",
        textAnimation           : "1.5s fadein",
        textAutoResize          : true,
        textResizeFactor        : 0.5 ,
        textColor               : "#202020",
        textClass               : "loading-text",
        textOrder               : 4
    });

    $.LoadingOverlay("show");

    // Hide it after 3 seconds
    setTimeout(function(){
        $.LoadingOverlay("hide");
    }, 3000);


</script>
<script>
    AOS.init({
        easing: 'ease-in-out-sine'
    });
</script>
<!-- end::Footer Scripts -->



<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
<script>
    // external js: isotope.pkgd.js


    // init Isotope
    var $grid = $('.grid').isotope({
        itemSelector: '.card-container',
        layoutMode: 'fitRows',
        getSortData: {
            name: '.name',
            symbol: '.symbol',
            number: '.number parseInt',
            category: '[data-category]',
            weight: function( itemElem ) {
                var weight = $( itemElem ).find('.weight').text();
                return parseFloat( weight.replace( /[\(\)]/g, '') );
            }
        }
    });

    // filter functions
    var filterFns = {
        // show if number is greater than 50
        numberGreaterThan50: function() {
            var number = $(this).find('.number').text();
            return parseInt( number, 10 ) > 50;
        },
        // show if name ends with -ium
        ium: function() {
            var name = $(this).find('.name').text();
            return name.match( /ium$/ );
        }
    };

    // bind filter button click
    $('#filters').on( 'click', 'a', function() {
        var filterValue = $( this ).attr('data-filter');
        // use filterFn if matches value
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({ filter: filterValue });
    });

    // bind sort button click
    $('#sorts').on( 'click', 'button', function() {
        var sortByValue = $(this).attr('data-sort-by');
        $grid.isotope({ sortBy: sortByValue });
    });

    // change is-checked class on buttons
    $('.button-group').each( function( i, buttonGroup ) {
        var $buttonGroup = $( buttonGroup );
        $buttonGroup.on( 'click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $( this ).addClass('is-checked');
        });
    });



</script>







</body>
</html>