<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>{{ $projectName }} | @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

   <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/assets/images/favicon/favicon.ico') }}">

    @yield('additional-css')

    <!-- plugins -->
    <link href="{{ asset('/assets/plugins/animate/animate.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dassets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{ asset('/dassets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
     @font-face {
            font-family: 'Ailerons';
            src: url("{{ asset('/assets/fonts/ailerons/ailerons-typeface-webfont.woff2') }}") format('woff2'),
            url("{{ asset('/assets/fonts/ailerons/ailerons-typeface-webfont.woff') }}") format('woff'),
            url("{{ asset('/assets/fonts/ailerons/ailerons-typeface-webfont.ttf') }}") format('truetype');
            font-weight: normal;
            font-style: normal;

        }
     html, body, h1,h2,h3,h4,h5 {
         color: #333;
         /*font-family: 'Poppins', sans-serif;*/
         font-size:14px;
         font-weight: 400;
         -font-family-sans-serif: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
         --font-family-monospace: SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;
     }
        .loading-text {
            font-size: 23px;
            font-family: Ailerons;
            text-align: center;
        }

        .link-line {
            display: inline-block;
            text-decoration: none;
        }

        .link-line::after {
            content: '';
            display: block;
            width: 0;
            height: 2px;
            background: #5369f8;
            transition: width .3s;
        }
        .link-line:hover::after {
            width: 100%;
        //transition: width .3s;
        }
        .curationcolor{ width: 30px; height:30px;  border-radius:50%; border:1px solid red; display: block; }


    </style>

</head>

<body>
<!-- Begin page -->
<div id="wrapper">

    <!-- Topbar Start -->
    <div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
        <div class="container-fluid">
            <!-- LOGO -->
            <a href="/dashboard" class="navbar-brand mr-0 mr-md-2 logo">
                        <span class="logo-lg">
                            <img src="{{ asset('/assets/images/logo.png') }}" alt="" height="50" />
                         </span>
                <span class="logo-sm">
                            <img src="{{ asset('/assets/images/logo.png') }}" alt="" height="50" />
                        </span>
            </a>

        </div>

    </div>
    <!-- end Topbar -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left-side-menu">
        <div class="media user-profile mt-2 mb-2" >
            @if(Auth::user()->file)
                <img src="{{ asset('assets/images/users/' . Auth::user()->file) }}" alt="" class="avatar-sm rounded-circle mr-2"/>

            @else

                <img src="{{ asset('assets/images/users/default.jpg') }}" alt="" class="avatar-sm rounded-circle mr-2"/>
            @endif
               <div class="media-body">
                <h6 class="pro-user-name mt-0 mb-0">{{ Auth::user()->name }}</h6>
                <span class="pro-user-desc">{{ Auth::user()->role->name }}</span>
            </div>
            <div class="dropdown align-self-center profile-dropdown-menu">
                <a class="dropdown-toggle mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                   aria-expanded="false">
                    <span data-feather="chevron-down"></span>
                </a>
                <div class="dropdown-menu profile-dropdown">
                    <a href="{{ route('users.index')  }}" class="dropdown-item notify-item">
                        <i data-feather="user" class="icon-dual icon-xs mr-2"></i>
                        <span>My Account</span>
                    </a>

                    <a href="/" class="dropdown-item notify-item">
                        <i data-feather="lock" class="icon-dual icon-xs mr-2"></i>
                        <span>Visit Website</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <a href="{{ route('logout') }}" class="dropdown-item notify-item" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                        <span>Logout</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </div>
            </div>
        </div>
        <div class="sidebar-content">
            <!--- Sidemenu -->
            <div id="sidebar-menu" class="slimscroll-menu">
                <ul class="metismenu" id="menu-bar">
                    <li class="menu-title">Navigation</li>

                    <li>
                        <a href="/dashboard">
                            <i data-feather="home"></i>
                            <span> Dashboard </span>
                        </a>
                    </li>
                    <li class="menu-title">Backend</li>
                    <li>
                        <a href="{{ route('users.index')  }}">
                            <i data-feather="users"></i>
                            <span> Manage Users </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript: void(0);">
                            <i data-feather="folder-minus"></i>
                            <span>Curation</span>
                            <span class="menu-arrow"></span>
                        </a>

                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{ route('curation-lists.create') }}">Curation Add</a>
                            </li>
                            <li>
                                <a href="{{ route('curation-lists.index') }}">Curation List</a>
                            </li>
                            <li>
                                <a href="{{ route('colors.index') }}">Curation Colours</a>
                            </li>
                            <li>
                                <a href="{{ route('curation-types.index') }}">Curation Types</a>
                            </li>
                            <li>
                                <a href="{{ route('curation-categories.index') }}">Curation Categories</a>
                            </li>
                         </ul>
                    </li>


                    <li>
                        <a href="javascript: void(0);">
                            <i data-feather="briefcase"></i>
                            <span>Capsules</span>
                            <span class="menu-arrow"></span>
                        </a>

                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{ route('capsule-brands.index') }}">Capsule Brands</a>
                            </li>
                            <li>
                                <a href="{{ route('capsule-lists.index') }}">Capsule List</a>
                            </li>
                            <li>
                                <a href="{{ route('merchants.index') }}">Featured Merchant</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('orders.index') }}">
                            <i data-feather="shopping-cart"></i>
                            <span>Capsule Orders</span>
                        </a>
                    </li>
                    <li class="menu-title">Frontend</li>

                    <li>
                        <a href="javascript: void(0);">
                            <i data-feather="settings"></i>
                            <span>Home Page</span>
                            <span class="menu-arrow"></span>
                        </a>

                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{ route('home-curations.index') }}">Curation</a>
                            </li>
                            <li>
                                <a href="{{ route('home-capsules.index') }}">Capsule</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="javascript: void(0);">
                            <i data-feather="briefcase"></i>
                            <span>Other</span>
                            <span class="menu-arrow"></span>
                        </a>

                        <ul class="nav-second-level" aria-expanded="false">
                            <li>
                                <a href="{{ route('website-contents.terms-content') }}">Terms Of Use</a>
                            </li>
                            <li>
                                <a href="{{ route('website-contents.privacy-content') }}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{ route('website-contents.about-content') }}">Our History</a>
                            </li>
                            <li>
                                <a href="{{ route('website-contents.contact-content') }}">Contact Us</a>
                            </li>
                            <li>
                                <a href="{{ route('website-contents.business-content') }}">Business</a>
                            </li>
                            <li>
                                <a href="{{ route('website-contents.support-content') }}">Support</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- End Sidebar -->

            <div class="clearfix"></div>
        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">
            @yield('content')
        </div> <!-- content -->



        <!-- Footer Start -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        &copy;Copyright  <?php echo date('Y'); ?> |  <strong>{{ $projectName }}</strong> | All Rights Reserved.
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- Right Sidebar -->
<div class="right-bar">
    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i data-feather="x-circle"></i>
        </a>
        <h5 class="m-0">Customization</h5>
    </div>

    <div class="slimscroll-menu">

        <h5 class="font-size-16 pl-3 mt-4">Choose Variation</h5>
        <div class="p-3">
            <h6>Default</h6>
            <a href="index.html"><img src="dassets/images/layouts/vertical.jpg" alt="vertical" class="img-thumbnail demo-img" /></a>
        </div>
        <div class="px-3 py-1">
            <h6>Top Nav</h6>
            <a href="layouts-horizontal.html"><img src="dassets/images/layouts/horizontal.jpg" alt="horizontal" class="img-thumbnail demo-img" /></a>
        </div>
        <div class="px-3 py-1">
            <h6>Dark Side Nav</h6>
            <a href="layouts-dark-sidebar.html"><img src="dassets/images/layouts/vertical-dark-sidebar.jpg" alt="dark sidenav" class="img-thumbnail demo-img" /></a>
        </div>
        <div class="px-3 py-1">
            <h6>Condensed Side Nav</h6>
            <a href="layouts-dark-sidebar.html"><img src="dassets/images/layouts/vertical-condensed.jpg" alt="condensed" class="img-thumbnail demo-img" /></a>
        </div>
        <div class="px-3 py-1">
            <h6>Fixed Width (Boxed)</h6>
            <a href="layouts-boxed.html"><img src="dassets/images/layouts/boxed.jpg" alt="boxed"
                                              class="img-thumbnail demo-img" /></a>
        </div>
    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="{{ asset('/dassets/js/vendor.min.js') }}"></script>

<!-- optional plugins -->
<script src="{{ asset('/dassets/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('/dassets/libs/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>

<!-- page js -->
<script src="{{ asset('dassets/js/pages/dashboard.init.js') }}"></script>

@yield('additional-js')

<!-- App js -->
<script src="{{ asset('/dassets/js/app.min.js') }}"></script>
<script src="{{ asset('/assets/plugins/descrambler/descrambler.min.js') }}"></script>

<script src="{{ asset('/assets/plugins/loadingoverlay/loadingoverlay.min.js') }}"></script>
<script>

    $.LoadingOverlaySetup({
        background              : "rgba(255, 255, 255, 0.9)",
        image                   : "",
        progress                : true,
        text                    : "CLTH",
        textAnimation           : "1.5s fadein",
        textAutoResize          : true,
        textResizeFactor        : 0.5 ,
        textColor               : "#202020",
        textClass               : "loading-text",
        textOrder               : 4
    });

    $.LoadingOverlay("show");

    // Hide it after 3 seconds
    setTimeout(function(){
        $.LoadingOverlay("hide");
    }, 3000);

    function goBack() {
        window.history.back();
    }
</script>

<script>
    $('.nav-second-level li a, #sidebar-menu > ul > li > a').addClass('link-line');
</script>

</body>
</html>
