@extends('layouts.dashboard')
@section('title', 'Add Curation')

@section('additional-css')

    <link href="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/select2/select2.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/multiselect/multi-select.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet"/>

    <link href="{{ asset('/dassets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/summernote/summernote-bs4.css') }}" rel="stylesheet"/>

@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Add Curation</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')

                <form action="{{ route('curation-lists.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-4 col-sm-4">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <label>Category</label>
                                                <select name="category_id" class="form-control">
                                                    <option value="" selected disabled hidden>Select Category</option>
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-sm-4">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <label>Type</label>
                                                <select name="type_id" class="form-control">
                                                    <option value="" selected disabled hidden>Select Type</option>
                                                    @foreach($types as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-xl-4 col-sm-4">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <label>Colors Filter</label>

                                                <select name="color_id" class="form-control">
                                                    <option value="" selected disabled hidden>Select Color</option>
                                                    @foreach($colors as $color)
                                                        <option value="{{ $color->id }}"
                                                                style="background: {{$color->hex }}; color:#fff;">{{ $color->name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </div> <!-- end card-body -->
                            </div> <!-- end card-->
                        </div> <!-- end col -->
                    </div>
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="title" placeholder="Title">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-12 col-form-label" for="description">Description</label>
                                        <div class="col-lg-12">
                                            <textarea class="form-control" rows="5" name="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-12 col-form-label" for="links">Links</label>
                                        <textarea id="summernote-editor" name="links">
                                   <ul>
<li>STYLE ITEM 1 - R900 - <strong><a href="https://www.mrp.com" target="_blank">Mr Price</a></strong></li>
<li>STYLE ITEM 2 - R400 - <strong><a href="https://www.yoco.co.za" target="_blank">Tshepo</a></strong></li>
<li>STYLE ITEM 3 - R299 - <strong><a href="https://www.zara.com/za/" target="_blank">ZARA</a></strong></li>
</ul>
                                 </textarea>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-12 col-form-label" for="description">Active?</label>
                                        <div class="col-lg-12">
                                            <div class="">
                                                <div class="custom-control custom-radio mb-2">
                                                    <input type="radio" id="customRadio1" name="is_active"
                                                           class="custom-control-input" value="1" checked>
                                                    <label class="custom-control-label" for="customRadio1">Yes</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio2" name="is_active"
                                                           class="custom-control-input" value="0">
                                                    <label class="custom-control-label" for="customRadio2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-block btn--md btn-primary" type="submit">Add Curation
                                    </button>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <label>Upload Curation Image <span style="color:red;">(440 X 660)</span></label>
                                        <input type="file" name="file"  class="form-control" />
                                    </div>

                                </div>

                            </div>


                        </div>
                </form>
            </div>

        </div> <!-- content -->

        @endSection


        @section('additional-js')
            <script src="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/select2/select2.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/multiselect/jquery.multi-select.js') }}"></script>
            <script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/dropzone/dropzone.min.js') }}"></script>
            <script src="{{ asset('/dassets/js/pages/form-advanced.init.js') }}"></script>
            <script src="{{ asset('/dassets/libs/summernote/summernote-bs4.min.js') }}"></script>
            <script src="{{ asset('dassets/js/pages/form-editor.init.js') }}"></script>


@endSection
