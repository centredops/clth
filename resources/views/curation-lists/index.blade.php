@extends('layouts.dashboard')
@section('title', 'Curation List')

@section('additional-css')
    <link href="{{ asset('/dassets/libs/datatables/dataTables.bootstrap4.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/dassets/libs/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />



@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Curation List</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Curation List</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-3">
                         <a href="{{ route('curation-lists.create') }}" class="btn btn-primary btn-block">Add New</a>
                        <br>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                              @if($curationLists->toArray())
                                <table id="basic-datatable" class="table dt-responsive nowrap">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>File</th>
                                        <th>Color</th>
                                        <th>Type</th>
                                        <th>Category</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    @foreach($curationLists as $curation)
                                    <tr>
                                        <td>{{$curation->title }}</td>
                                        <td><img src="{{ asset('/assets/images/curation/' . $curation->file) }}" alt="{{$curation->title }}" class="img-fluid" style="max-width: 30px;" /> </td>
                                        <td class="text-center"><div style="width: 30px; height:30px; background: {{$curation->color->hex }}; border-radius:50%;"></div></td>
                                        <td>{{$curation->type->name }}</td>
                                        <td>{{$curation->category->name }}</td>
                                        <td class="text-center"> <a href="{{ route('curation-lists.edit', $curation->id) }}" class="btn btn-success btn-block">Edit</a></td>
                                        <td>
                                            <form action="{{url('curation-lists', [$curation->id])}}" method="POST">
                                                {{method_field('DELETE')}}
                                                @csrf
                                                <input type="submit" class="btn btn-danger btn-block" value="Delete"/>
                                            </form>
                                        </td>
                                    </tr>
                                     @endforeach
                                     </tbody>
                                </table>
                               @endif

                            </div> <!-- end card body-->
                        </div> <!-- end card -->
                    </div><!-- end col-->
                </div>

            </div>

        </div> <!-- content -->
    @endSection


    @section('additional-js')

        <!-- datatable js -->
            <script src="{{ asset('/dassets/libs/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('dassets/libs/datatables/dataTables.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/dataTables.responsive.min.js') }}"></script>
            <script src="{{ asset('/assets/libs/datatables/responsive.bootstrap4.min.js') }}"></script>

            <script src="{{ asset('/dassets/libs/datatables/dataTables.buttons.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.bootstrap4.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.html5.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.flash.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/buttons.print.min.js') }}"></script>

            <script src="{{ asset('/dassets/libs/datatables/dataTables.keyTable.min.js') }}"></script>
            <script src="{{ asset('/dassets/libs/datatables/dataTables.select.min.js') }}"></script>



            <!-- Datatables init -->
            <script src="{{ asset('/dassets/js/pages/datatables.init.js') }}"></script>
            <script src="{{ asset('dassets/js/pages/form-advanced.init.js') }}"></script>

@endSection
