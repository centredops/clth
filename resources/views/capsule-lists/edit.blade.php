@extends('layouts.dashboard')
@section('title', 'Edit Capsule')

@section('additional-css')

    <link href="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/select2/select2.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/multiselect/multi-select.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.css') }}" rel="stylesheet"/>

    <link href="{{ asset('/dassets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('/dassets/libs/summernote/summernote-bs4.css') }}" rel="stylesheet"/>

@endSection()



@section('content')
    <div class="content">

        <!-- Start Content-->
        <div div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row page-title">
                    <div class="col-md-12">
                        <nav aria-label="breadcrumb" class="float-right mt-1">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">{{ $projectName }}</a></li>
                                <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Products</li>
                            </ol>
                        </nav>
                        <h4 class="mb-1 mt-0">Add Capsule</h4>
                    </div>
                </div>
                @include('layouts.includes.success')
                @include('layouts.includes.errors')

                <form action="{{ route('capsule-lists.update', $capsule->id) }}" method="POST"
                      enctype="multipart/form-data">

                    {{ csrf_field() }}

                    {{ method_field('PATCH') }}

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6 col-sm-6">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <label>Category</label>
                                                <select name="brand_id" class="form-control">
                                                    <option value="" selected disabled hidden>Select Brand</option>
                                                    @foreach($brands as $brand)
                                                        <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-6 col-sm-6">
                                            <div class="form-group mt-3 mt-sm-0">
                                                <label>Sizes</label>
                                                <input type="text" class="form-control" name="size_id" value="{{ $capsule->size_id }}">
                                            </div>
                                        </div>
                                    </div>

                                </div> <!-- end card-body -->
                            </div> <!-- end card-->
                        </div> <!-- end col -->
                    </div>
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="title"
                                                   value="{{ $capsule->title }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="details"
                                                   value="{{ $capsule->details }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="code"
                                                   value="{{ $capsule->code }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control" name="price"
                                                   value="{{ $capsule->price }}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-12 col-form-label" for="description">Description</label>
                                        <div class="col-lg-12">
                                            <textarea class="form-control" rows="5" name="description">
                                               {{ $capsule->description }}
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-12 col-form-label" for="description">Active?</label>
                                        <div class="col-lg-12">
                                            <div class="">
                                                <div class="custom-control custom-radio mb-2">
                                                    <input type="radio" id="customRadio1" name="is_active"
                                                           class="custom-control-input"
                                                           value="1" {{ $capsule->is_active == '1' ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="customRadio1">Yes</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="customRadio2" name="is_active"
                                                           class="custom-control-input"
                                                           value="0" {{ $capsule->is_active == '0' ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="customRadio2">No</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-block btn--md btn-primary" type="submit">Edit Capsule
                                    </button>

                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <label>Upload Featured Image</label>
                                        <input type="file" name="file" class="form-control"/>
                                        <p>&nbsp;</p>
                                        <label>Current Capsule Image</label>
                                        <p><img src="{{ asset('/assets/images/capsules/' . $capsule->file ) }}"
                                                class="img-fluid block m-auto"/></p>
                                    </div>

                                </div>

                            </div>


                        </div>
                </form>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="col-md-12">
                                <label>Upload {{ $capsule->title }} Gallery:</label>
                            </div>
                            <div class="col-12">


                                <form action="{{ route('capsule-list.add-gallery', $capsule->id) }}" name="file"
                                      method="post" class="dropzone" id="fileUploads" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    {{ method_field('PATCH') }}

                                    <input type="hidden" name="file_id" value="{{ $capsule->id }}">
                                    <input type="hidden" name="type" value="capsule_gallery">
                                    <div class="fallback">
                                        <input name="file" type="file" id="file" multiple/>
                                    </div>

                                    <div class="dz-message needsclick">
                                        <i class="h1 text-muted  uil-cloud-upload"></i>
                                        <h3>Drop files here or click to upload.</h3>
                                        <span class="text-muted font-13">(Please drop all <strong>{{ $capsule->title }} Gallery Images</strong></span>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>


                </div>
            </div>

            <div class="row">
                @if($capsuleGallery->isEmpty())
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body text-center">
                                <p>There are not galleries, default banner will display</p>
                            </div>
                        </div>
                    </div>
                    @else

                    @foreach($capsuleGallery as $image)
                        <div class="card mb-3 mb-xl-0">
                            <img class="card-img-top img-fluid"
                                 src="{{ asset('assets/images/capsule-gallery/' . $image->file ) }}" alt=""
                                 style="max-width: 400px;">
                            <div class="card-body">
                                <form action="{{ route('capsule-list.delete-gallery',$image->id) }}" method="POST">
                                    @csrf
                                    <input type="submit" class="btn btn-danger btn-block" value="Delete"/>
                                </form>
                            </div>
                        </div>
                    @endforeach

                @endif


            </div>

        </div>

    </div> <!-- content -->

@endSection


@section('additional-js')
    <script src="{{ asset('/dassets/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('/dassets/libs/flatpickr/flatpickr.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('/dassets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('/dassets/js/pages/form-advanced.init.js') }}"></script>
    <script src="{{ asset('/dassets/libs/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('dassets/js/pages/form-editor.init.js') }}"></script>


@endSection
