<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*
|--------------------------------------------------------------------------
| Frontend
|--------------------------------------------------------------------------
|*/

Route::get('/', 'FrontController@index')->name('front.index');

Route::get('/404', 'FrontController@notFound')->name('front.404');

Route::get('/curation', 'FrontController@curation')->name('front.curation');

Route::get('/curation/content-content', 'FrontController@curation')->name('front.curation-content');

Route::get('/curation-post/{id}', 'FrontController@curationPost')->name('front.curation-post');

Route::get('/capsule', 'FrontController@capsule')->name('front.capsule');

Route::get('/capsule-details/{id}', 'FrontController@capsuleDetails')->name('front.capsule-details');

Route::get('/service-terms', 'FrontController@serviceTerms')->name('front.service-terms');

Route::get('/privacy-policy', 'FrontController@privacyPolicy')->name('front.privacy-policy');

Route::get('/about-us', 'FrontController@aboutUs')->name('front.about-us');

Route::get('/business', 'FrontController@business')->name('front.business');


//Auth Routes
Route::group(['middleware' => ['auth']], function () {
    /*Like*/
    Route::post('/likes/post/', 'LikesController@like')->name('likes.like');
    Route::post('/likes/{id}', 'LikesController@dislike')->name('likes.dislike');


    /*Shopping Bag*/
    Route::get('/shopping-bag', 'FrontController@shoppingBag')->name('front.shopping-bag');
    Route::get('/add-shopping-bag/{id}', 'FrontController@addShoppingBag')->name('front.add-shopping-bag');
    Route::patch('update-shopping-bag', 'FrontController@updateShoppingBag');
    Route::delete('remove-shopping-bag', 'FrontController@removeShoppingBag');
    Route::get('/checkout-bag', 'FrontController@checkoutBag')->name('front.checkout-bag');
    Route::get('/place-order/{id}', 'FrontController@placeOrder')->name('front.place-order');
    Route::get('/place-order/mail', 'FrontController@placeOrderMail')->name('front.order-mail');
    Route::get('/order-details', 'FrontController@orderDetails')->name('front.order-details');
    Route::get('/continue-shopping', 'FrontController@continueShopping')->name('front.continue-shopping');
    Route::get('/track-order', 'FrontController@trackOrder')->name('front.track-order');

    Route::get('/shopping-closet', 'ShoppingClosetsController@shoppingCloset')->name('front.shopping-closet');

    Route::get('/shopping-closet/{id}', 'ShoppingClosetsController@updateCustomer')->name('front.update-customer');

    Route::post('/my-profile/{id}', 'ShoppingClosetsController@myProfile')->name('front.my-profile');

    Route::get('/contact-us', 'FrontController@contactUs')->name('front.contact-us');

    Route::get('/support', 'FrontController@support')->name('front.support');
});


/*Facebook login*/
Route::get('login/facebook', 'Auth\LoginController@redirectToProviderFacebook')->name('login.facebook');
Route::get('facebook/callback', 'Auth\LoginController@handleProviderCallbackFacebook');

/*Twitter login*/
Route::get('login/twitter', 'Auth\LoginController@redirectToProviderTwitter')->name('login.twitter');
Route::get('twitter/callback', 'Auth\LoginController@handleProviderCallbackTwitter');

/*Google login*/
Route::get('login/google', 'Auth\LoginController@redirectToProviderGoogle')->name('login.google');
Route::get('auth/google/callback', 'Auth\LoginController@handleProviderCallbackGoogle');


/*
|--------------------------------------------------------------------------
| Dashboard
|--------------------------------------------------------------------------
|*/

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => ['auth', 'dashboard-access']], function () {
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');
    Route::get('/profile', 'DashboardController@myProfile')->name('dashboard.profile');

    /*Curation Colors*/
    Route::resource('/colors', 'CurationColorsController');

    /* Curation Categories*/

    Route::get('/categories/index', 'CurationCategoriesController@index')->name('curation-categories.index');
    Route::get('/add-category', 'CurationCategoriesController@addCategory')->name('curation-categories.add-category');
    Route::get('/edit-category/{id}', 'CurationCategoriesController@editCategory')->name('curation-categories.edit-category');
    Route::get('/store-category', 'CurationCategoriesController@store')->name('curation-categories.store-category');
    Route::get('/delete-category/{id}', 'CurationCategoriesController@deleteCategory')->name('curation-categories.delete-category');

    /*Curation list*/
    Route::resource('/curation-lists', 'CurationListsController');

    /*Curation Types*/
    Route::resource('/curation-types', 'CurationTypesController');

    /*Capsule Brands*/
    Route::resource('/capsule-brands', 'CapsuleBrandsController');

    /*Capsule Sizes*/
    Route::resource('/capsule-sizes', 'CapsuleSizesController');

    /*Capsule Lists*/
    Route::resource('/capsule-lists', 'CapsuleListsController');

    Route::patch('/capsule-lists/{id}/edit', 'CapsuleListsController@addGallery')->name('capsule-list.add-gallery');
    Route::post('/capsule-lists/{id}/delete-gallery', 'CapsuleListsController@deleteGallery')->name('capsule-list.delete-gallery');


    /*Home Curations*/
    Route::resource('/home-curations', 'HomeCurationsController');

    /*Orders*/
    Route::resource('/orders', 'OrdersController');

    /*Merchants*/
    Route::resource('/merchants', 'MerchantsController');
    Route::post('/merchants/{id}', 'MerchantsController@deleteImage')->name('merchants.delete-image');

    /*Home Capsules*/
    Route::resource('/home-capsules', 'HomeCapsulesController');
    Route::post('/home-capsules/{id}', 'HomeCapsulesController@deleteImage')->name('home-capsules.delete-image');

    /*Website Content*/
    Route::get('/website-contents/terms-content', 'WebsiteContentsController@termsContent')->name('website-contents.terms-content');
    Route::post('/website-contents/terms-content/create', 'WebsiteContentsController@termsCreate')->name('website-contents.terms-create');

    Route::get('/website-contents/privacy-content', 'WebsiteContentsController@privacyContent')->name('website-contents.privacy-content');
    Route::post('/website-contents/privacy-content/create', 'WebsiteContentsController@privacyCreate')->name('website-contents.privacy-create');

    Route::get('/website-contents/about-content', 'WebsiteContentsController@aboutContent')->name('website-contents.about-content');
    Route::post('/website-contents/about-content/create', 'WebsiteContentsController@aboutCreate')->name('website-contents.about-create');

    Route::get('/website-contents/contact-content', 'WebsiteContentsController@contactContent')->name('website-contents.contact-content');
    Route::post('/website-contents/contact-content/create', 'WebsiteContentsController@contactCreate')->name('website-contents.contact-create');

    Route::get('/website-contents/support-content', 'WebsiteContentsController@supportContent')->name('website-contents.support-content');
    Route::post('/website-contents/support-content/create', 'WebsiteContentsController@supportCreate')->name('website-contents.support-create');

    Route::get('/website-contents/business-content', 'WebsiteContentsController@businessContent')->name('website-contents.business-content');
    Route::post('/website-contents/business-content/create', 'WebsiteContentsController@businessCreate')->name('website-contents.business-create');


    /*Manage Users*/
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::post('/users/{id}', 'UsersController@updateMyProfile')->name('users.update-my-profile');
    Route::get('/users/edit-user/{id}', 'UsersController@editUser')->name('users.edit-user');
    Route::post('/users/{id}/update-user', 'UsersController@updateUser')->name('users.update-user');
    Route::get('/users/{id}/delete-user', 'UsersController@deleteUser')->name('users.delete-user');

});


Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::get('/updateapp', function () {
    \Artisan::call('dump-autoload');
    echo 'dump-autoload complete';
});






