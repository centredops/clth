<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['user_id','order_number', 'title',  'file',  'details','code', 'description','quantity', 'total','status', 'payment_method'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    protected $primaryKey = "user_id";
}
