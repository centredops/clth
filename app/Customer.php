<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['user_id', 'address', 'city', 'postal_code', 'province'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
