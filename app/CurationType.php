<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurationType extends Model
{
    protected $fillable = ['name'];
}
