<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CapsuleList extends Model
{
    protected $fillable = [
        'title',
        'details',
        'code',
        'description',
        'file',
        'price',
        'size_id',
        'brand_id',
        'is_active'
    ];

    public function size(){
        return $this->belongsTo('App\CapsuleSize');
    }

    public function brand(){
        return $this->belongsTo('App\CapsuleBrand');
    }


}
