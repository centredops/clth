<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CapsuleSize extends Model
{
    protected $fillable = ['name'];
}
