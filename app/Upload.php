<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use symfony\symfony\Component\HttpFoundation\File\UploadedFile;

class Upload extends Model
{
    protected $fillable = ['file', 'file_id', 'type'];
}
