<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()

      {


        if($this->app->environment('production'))
        {
            $this->app['request']->server->set('HTTPS','on');
        }

        $projectName = "CLTH";

        view::share(['projectName' => $projectName]);
    }
}
