<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeCapsule extends Model
{
    protected $fillable = ['description'];
}
