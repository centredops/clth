<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurationColor extends Model
{
    protected $fillable = ['name', 'hex'];
}
