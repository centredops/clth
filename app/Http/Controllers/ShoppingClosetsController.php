<?php

namespace App\Http\Controllers;


use App\Customer;
use App\Http\Requests\UserProfileUpdateRequest;
use App\User;
use App \ {CurationList, Like, Order};

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ShoppingClosetsController extends Controller
{
    public function shoppingCloset()
    {

        $user_id = auth()->user()->id;

        $orders = Order::where('user_id','=', $user_id)->where('status', '=', 'Pending')->get();

        $cancelled_orders = Order::where('user_id','=', $user_id)->where('status', '=', 'Cancelled')->get();

        $order_history = Order::where('user_id','=', $user_id)->paginate(6);

        $curation_id = Like::where('user_id', '=', $user_id)
            ->where('like', '=', 1)
            ->pluck('curation_id')->all();

        $curations = CurationList::whereIn('id', $curation_id)
            ->orderByRaw(CurationList::raw("FIELD(id, " . implode(",", $curation_id) . ")"))
            ->paginate(4);



        $user_id = auth()->user()->id;

        $userAlready = Customer::where('user_id', '=', $user_id)->exists();

        if($userAlready){

            $customer_details = Customer::where('user_id', $user_id)->get();

        }else{

            $input = new Customer([
                'user_id' => $user_id
            ]);

            $input->save();
        }

        $customer_details = Customer::where('user_id', $user_id)->get();


        return view('front.shopping-closet', compact('curations', 'orders', 'order_history', 'cancelled_orders', 'customer_details'));

    }

    public function updateCustomer(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'email' => 'email',
            'cell' => 'required|numeric',
            'payment_method' => 'required',
            'address' => 'required',
        ]);

        $input = $request->all();

        // dd($input);

        $userAlready = Customer::where('user_id', '=', $id)->first();

        //dd($userAlready);

        $userAlready->update($input);


        return redirect()->back()->with('success', 'Details updated');

    }

    public function myProfile(UserProfileUpdateRequest $request, $id)
    {

        $input = $request->all();

        $myProfile = User::findorfail($id);

        $input['password'] = bcrypt($request->get('password'));

        $myProfile->update($input);

        return redirect(route('front.shopping-closet'))->with('success', 'Your Profile Has Been Updated');
    }

}
