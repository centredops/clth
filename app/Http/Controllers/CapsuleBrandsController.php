<?php

namespace App\Http\Controllers;

use App\CapsuleBrand;
use Illuminate\Http\Request;

class CapsuleBrandsController extends Controller
{
    public function index()
    {

        $brands = CapsuleBrand::all();

        return view('capsule-brands.index', compact('brands'));
    }

    public function store(Request $request)
    {

        $input = $request->all();

        CapsuleBrand::create($input);

        return redirect(route('capsule-brands.index'))->with('success', 'Brand Has Been Added');
    }


    public function edit($id)
    {

        $brand = CapsuleBrand::findOrFail($id);

        return view('capsule-brands.edit', compact('brand'));
    }

    public function update(Request $request, $id)
    {

        $input = $request->all();

        $brand = CapsuleBrand::findorfail($id);


        $brand->update($input);

        return redirect(route('capsule-brands.index'))->with('success', 'Brand Has Been Updated');
    }

    public function destroy($id)
    {
        $input = CapsuleBrand::findOrFail($id);

        $input->delete();

        return redirect(route('capsule-brands.index'))->with('success', 'Brand Has Been Deleted!');
    }
}
