<?php

namespace App\Http\Controllers;

use App\Http\Requests\CapsuleCreateRequest;
use App\Http\Requests\CapsuleUpdateRequest;
use App\Upload;
use App \ {CapsuleList, CapsuleSize, CapsuleBrand};
use File;
use Illuminate\Http\Request;

class CapsuleListsController extends Controller
{
    public function index()
    {
        $capsuleLists = CapsuleList::all();

        return view ('capsule-lists.index', compact('capsuleLists'));
    }


    public function create()
    {
        $brands = CapsuleBrand::all();

        //dd($sizes);

        return view('capsule-lists.create', compact('brands'));
    }


    public function store(CapsuleCreateRequest $request)
    {
        $input = $request->all();

        if($file = $request->file('file')){
            $fileName = strtolower( time() . '_' . $file->getClientOriginalName());;

            $file->move(public_path('assets/images/capsules/'),$fileName);

            $input['file'] = $fileName;
        }

        CapsuleList::create($input);

        return redirect(route('capsule-lists.create'))->with('success', 'Capsule Has Been Added');

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $capsule = CapsuleList::findOrFail($id);
        $brands = CapsuleBrand::all();

        $capsuleGallery = Upload::where('file_id', '=', $id)->where('type', '=', 'capsule_gallery')->get();

        return view('capsule-lists.edit', compact('capsule', 'brands', 'capsuleGallery'));
    }


    public function update(CapsuleUpdateRequest $request, $id)
    {

        $input = $request->all();

        $capsule = CapsuleList::findorfail($id);

        if($file = $request->file('file')){
            $fileName = strtolower( time() . '_' . $file->getClientOriginalName());;

            $file->move(public_path('assets/images/capsules/'),$fileName);

            $input['file'] = $fileName;
        }

        $capsule->update($input);

        return redirect(route('capsule-lists.index'))->with('success', 'Capsule Has Been Updated');
    }


    public function destroy($id)
    {
        $input = CapsuleList::findOrFail($id);

        $fileName = $input->file;

        if(File::exists(public_path('assets/images/capsules/') . $fileName)){
            unlink(public_path('assets/images/capsules/') . $fileName);
        }


        $input->delete();

        return redirect(route('capsule-lists.index'))->with('success', 'Capsule Has Been Deleted!');
    }



    public function addGallery(Request $request)
    {
        {
            //dd($request->all());
            $image = $request->file('file');
            $fileName = $image->getClientOriginalName();
            $image->move(public_path('/assets/images/capsule-gallery/'),$fileName);
            $fileUpload = new Upload();
            $fileUpload->file = $fileName;
            $fileUpload->file_id = $request->get('file_id');
            $fileUpload->type = $request->get('type');
            $fileUpload->save();
            //return response()->json(['success'=>$fileName]);
            return redirect()->back();
        }
    }

    public function deleteGallery($id)
    {
        $input = Upload::findOrFail($id);

        $fileName = $input->file;

        if(File::exists(public_path('assets/images/capsule-gallery/') . $fileName)){
            unlink(public_path('assets/images/capsule-gallery/') . $fileName);
        }

        $input->delete();

        return redirect()->back();
    }
}
