<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;

class OrdersController extends Controller
{

    public function index(){

        $orders = Order::get()->unique('order_number');

        return view('orders.index', compact('orders'));
    }

    public function show($id){

        $order = Order::where('order_number','=', $id)->get();

        //dd($order->toArray());

        $orderDetails = Order::where('order_number','=', $id)->get();

       return view('orders.show', compact('order', 'orderDetails'));


    }

    public function update(Request $request, $id){

        $input = $request->all();

        //$order = Order::where('order_number',$id)->first();
         Order::where('order_number', '=', $id)->update(array('status' => $request->status));


        //order->update($input);

        return redirect()->back()->with('success', 'Order Status Updated');
    }
}
