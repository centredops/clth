<?php

namespace App\Http\Controllers;


use App\Http\Requests\UserProfileUpdateRequest;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(){
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function updateMyProfile(UserProfileUpdateRequest $request, $id)
    {

        $input = $request->all();

        $myProfile = User::findorfail($id);

        if($file = $request->file('file')){
            $fileName = strtolower( time() . '_' . $file->getClientOriginalName());

            $file->move(public_path('assets/images/users/'),$fileName);

            $input['file'] = $fileName;
        }

        $input['password'] = bcrypt($request->get('password'));

        $myProfile->update($input);

        return redirect(route('users.index'))->with('success', 'Your Profile Has Been Updated');
    }

    public function deleteUserProfile($id)
    {
        $input = User::findOrFail($id);

        $input->delete();

        return redirect(route('users.index'))->with('success', 'User Has Been Deleted');
    }

    public function editUser($id)
    {

        $showUser = User::findorfail($id);

        return view('users.edit', compact('showUser'));
    }

    public function updateUser(Request $request, $id)
    {
        $input = $request->all();

        $updateUser = User::findorfail($id);


        $updateUser->update($input);

        return redirect(route('users.index'))->with('success', 'User Has Been Deleted');
    }

    public function deleteUser($id)
    {
        $input = User::findOrFail($id);

        $input->delete();

        return redirect(route('users.index'))->with('success', 'User Has Been Deleted');
    }
}
