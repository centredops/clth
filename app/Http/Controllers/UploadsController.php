<?php

namespace App\Http\Controllers;

use App\Upload;
use Illuminate\Http\Request;

class UploadsController extends Controller
{
    public function index(){

        return view('uploads.index');

    }

    public function store(Request $request)
    {
        //dd($request->all());
        $image = $request->file('file');
        $fileName = $image->getClientOriginalName();
        $image->move(public_path('/assets/images/uploads/'),$fileName);

        $fileUpload = new Upload();
        $fileUpload->file = $fileName;
        $fileUpload->file_id = $fileName;
        $fileUpload->save();
        return response()->json(['success'=>$fileName]);
    }
}
