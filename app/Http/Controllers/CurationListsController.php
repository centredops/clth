<?php

namespace App\Http\Controllers;


use App\Http\Requests\CurationCreateRequest;
use App\Http\Requests\CurationUpdateRequest;
use Illuminate\Http\Request;
use File;

use App \ {CurationList, CurationCategory, CurationColor, CurationType};

class CurationListsController extends Controller
{

    public function index()
    {
     $curationLists = CurationList::all();
     return view ('curation-lists.index', compact('curationLists'));
    }


    public function create()
    {
        $categories = CurationCategory::all();
        $colors = CurationColor::all();
        $types = CurationType::all();
        //dd($categories);

        return view('curation-lists.create', compact('categories', 'colors', 'types'));
    }


    public function store(CurationCreateRequest $request)
    {
        $input = $request->all();

        if($file = $request->file('file')){
            $fileName = strtolower( time() . '_' . $file->getClientOriginalName());;

            $file->move(public_path('assets/images/curation/'),$fileName);

            $input['file'] = $fileName;
         }

       CurationList::create($input);

      return redirect(route('curation-lists.create'))->with('success', 'Curation Has Been Added');

    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $curation = CurationList::findOrFail($id);
        $categories = CurationCategory::all();
        $colors = CurationColor::all();
        $types = CurationType::all();
        return view('curation-lists.edit', compact('curation', 'categories', 'colors', 'types'));
    }


    public function update(CurationUpdateRequest $request, $id)
    {

        $input = $request->all();

        $curation = CurationList::findorfail($id);

        if($file = $request->file('file')){
            $fileName = strtolower( time() . '_' . $file->getClientOriginalName());;

            $file->move(public_path('assets/images/curation/'),$fileName);

            $input['file'] = $fileName;
        }

        $curation->update($input);

        return redirect(route('curation-lists.index'))->with('success', 'Curation Has Been Updated');
    }


    public function destroy($id)
    {
        $input = CurationList::findOrFail($id);

        $fileName = $input->file;

        if(File::exists(public_path('assets/images/curation/') . $fileName)){
            unlink(public_path('assets/images/curation/') . $fileName);
        }


        $input->delete();

        return redirect(route('curation-categories.index'))->with('success', 'Curation Has Been Deleted!');
    }
}
