<?php

namespace App\Http\Controllers;

use App\CurationType;
use Illuminate\Http\Request;

class CurationTypesController extends Controller
{
    public function index()
    {

        $types = CurationType::all();

        return view('curation-types.index', compact('types'));
    }

    public function edit($id)
    {

        $type = CurationType::findOrFail($id);

        return view('curation-types.edit', compact('type'));
    }


    public function store(Request $request)
    {

        $input = $request->all();


        CurationType::create($input);

        return redirect(route('curation-types.index'))->with('success', 'Type Has Been Added');
    }

    public function update(Request $request, $id)
    {

        $input = $request->all();

        $type = CurationType::findorfail($id);


        $type->update($input);

        return redirect(route('curation-types.index'))->with('success', 'Type Has Been Updated');
    }

    public function destroy($id)
    {
        $input = CurationType::findOrFail($id);

        $input->delete();

        return redirect(route('curation-types.index'))->with('success', 'Type Has Been Deleted!');
    }


}
