<?php

namespace App\Http\Controllers;

use App\CurationCategory;
use Illuminate\Http\Request;
use Redirect,Response;


class CurationCategoriesController extends Controller
{
    public function index()
    {
        $categories = CurationCategory::all();
        return view('curation-categories.index', compact('categories'));
    }

    public function addCategory(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        CurationCategory::create($request->all());
        return redirect(route('curation-categories.index'))->with('success', 'Category Added successful!');
    }


    public function editCategory($id)
    {
        $where = array('id' => $id);
        $category  = CurationCategory::where($where)->first();

        return Response::json($category);


    }


    public function store(Request $request)
    {
        $categoryID = $request->category_id;
        $category   =   CurationCategory::updateOrCreate(['id' => $categoryID],
            ['name' => $request->name, 'description' => $request->description]);

        $request->session()->flash('success', 'Category Updated Successfully!');
        $request->session()->flash('message-type', 'success');

        return Response::json($category);
    }


    public function deleteCategory($id)
    {
        $category = CurationCategory::findOrFail($id);
        $category->delete();

        return redirect(route('curation-categories.index'))->with('success', 'Category Deleted!');
    }
}
