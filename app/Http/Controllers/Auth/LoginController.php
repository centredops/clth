<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Socialite;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    //protected $redirectTo = RouteServiceProvider::HOME;

    public function redirectTo(){

        // User role
        $role = Auth::user()->role->name;

        // Check user role
        switch ($role) {
            case 'Administrator':
                return '/dashboard';
                break;
            case 'Subscriber':
                return '/';
                break;
            default:
                return '/';
                break;
        }
    }

     //Facebook Login
     public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

     public function handleProviderCallbackFacebook()
    {
        $userSocial  = Socialite::driver('facebook')->user();

        //dd($userSocial);

        //Check if Facebook user exists and log user in
        $user = User::where('email', $userSocial->user['email'])->first();

        if($user){
            if(Auth::loginUsingId($user->id)){
                return redirect()->back();
            }
        }

        //else sign the user up
        $userSignup = User::create([
            'name' => $userSocial->user['name'],
            'email' => $userSocial->user['email'],
            'password' => bcrypt('1234'),
            'file'=> $userSocial->avatar

        ]);

        //finally log the user in
        if($userSignup){
            if(Auth::loginUsingId($userSignup->id)){
                return redirect()->route('front.shopping-closet');
            }
        }


    }

    //Twitter Login
    public function redirectToProviderTwitter()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function handleProviderCallbackTwitter()
    {
        $userSocial  = Socialite::driver('twitter')->user();

        //dd($userSocial);

        //Check if Facebook user exists and log user in
        $user = User::where('email', $userSocial->email)->first();
        //dd($user);

        if($user){
            if(Auth::loginUsingId($user->id)){
                return redirect()->back();
            }
        }

        //else sign the user up
        $userSignup = User::create([
            'name' => $userSocial->name,
            'email' => $userSocial->email,
            'password' => bcrypt('1234'),
            'file'=> $userSocial->avatar

        ]);

        //finally log the user in
        if($userSignup){
            if(Auth::loginUsingId($userSignup->id)){
                return redirect()->route('front.shopping-closet');
            }
        }


    }


    //Google Login
    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallbackGoogle()
    {
        $userSocial  = Socialite::driver('google')->user();

        dd($userSocial);

        //Check if Facebook user exists and log user in
        $user = User::where('email', $userSocial->email)->first();
        //dd($user);

        if($user){
            if(Auth::loginUsingId($user->id)){
                return redirect()->back();
            }
        }

        //else sign the user up
        $userSignup = User::create([
            'name' => $userSocial->name,
            'email' => $userSocial->email,
            'password' => bcrypt('1234')
            //'file'=> $userSocial->avatar

        ]);

        //finally log the user in
        if($userSignup){
            if(Auth::loginUsingId($userSignup->id)){
                return redirect()->route('front.shopping-closet');
            }
        }


    }


    protected function credentials(Request $request)
    {
        if(is_numeric($request->get('email'))){
            return ['cell'=>$request->get('email'),'password'=>$request->get('password')];
        }

        return $request->only($this->username(), 'password');
    }




    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
