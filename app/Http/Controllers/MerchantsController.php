<?php

namespace App\Http\Controllers;

use App\Merchant;
use App\Upload;
use Illuminate\Http\Request;
use File;

class MerchantsController extends Controller
{
    public function index(){
        $merchants = Merchant::all();
        $images = Upload::where('type', '=', 'featured_merchant')->get();
        return view ('merchants.index', compact('merchants', 'images'));
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'file' => 'image|mimes:jpeg,png,jpg'
        ]);

        $input = $request->all();

        if($file = $request->file('file')){
            $fileName = strtolower( time() . '_' . $file->getClientOriginalName());;

            $file->move(public_path('assets/images/merchants/'),$fileName);

            $input['file'] = $fileName;
        }

        Merchant::create($input);

        return redirect(route('merchants.index'))->with('success', 'Merchant Has Been Added');

    }
    public function destroy($id)
    {
        $input = Merchant::findOrFail($id);


        $fileName = $input->file;

       // dd($fileName);

        if(File::exists(public_path('assets/images/merchants/') . $fileName)){
            unlink(public_path('assets/images/merchants/') . $fileName);
        }

        $input->delete();

        return redirect(route('merchants.index'))->with('success', 'Merchant Has Been Deleted');
    }

    public function update(Request $request)
    {
        {
            //dd($request->all());
            $image = $request->file('file');
            $fileName = $image->getClientOriginalName();
            $image->move(public_path('/assets/images/capsule-featured-list/'),$fileName);

            $fileUpload = new Upload();
            $fileUpload->file = $fileName;
            $fileUpload->file_id = $request->get('file_id');
            $fileUpload->type = $request->get('type');
            $fileUpload->save();
            //return response()->json(['success'=>$fileName]);
            return redirect(route('merchants.index'))->with('success', 'Merchant Sliders uploaded');
        }
    }


    public function deleteImage($id)
    {
        $input = Upload::findOrFail($id);

        $fileName = $input->file;

        //dd($fileName);

        if(File::exists(public_path('assets/images/capsule-featured-list/') . $fileName)){
            unlink(public_path('assets/images/capsule-featured-list/') . $fileName);
        }

        $input->delete();

        return redirect(route('merchants.index'))->with('success', 'Image Deleted');
    }



}
