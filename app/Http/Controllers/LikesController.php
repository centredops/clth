<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;

class LikesController extends Controller
{

    public function like(Request $request)
    {

        $input = $request->all();

        $input['user_id'] = auth()->user()->id;

        $id = $request->curation_id;

        $contentAvailable = Like::where('curation_id', $id)->exists();

        if ($contentAvailable) {

            $updateContent = Like::findOrFail($id);

            $updateContent->update($input);

        } else {

            Like::create($input);

        }

        return redirect()->back();
    }
}
