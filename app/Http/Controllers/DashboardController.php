<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App \ {Order, Like, CurationList, User};

class DashboardController extends Controller
{
    public function index()
    {
        $orders = Order::get()->unique('order_number');

        $total_revenue = Order::sum('total');

        $total_orders = Order::get()->unique('order_number')->count();

        $pending_orders = Order::get()->unique('order_number')->where('status', '=', 'Pending')->count();

        $approved_orders = Order::get()->unique('order_number')->where('status', '=', 'Approved')->count();

        $curation_likes = Like::get()->unique('like')->count();

        $curation_list = CurationList::count();

        $total_users = User::count();


        //dd($total_revenue);


        return view('dashboard.index', compact(
            'orders',
            'total_revenue',
            'total_orders',
            'pending_orders',
            'approved_orders',
            'curation_likes',
            'curation_list',
            'total_users'));
    }

    public function myProfile()
    {
        return view('dashboard.profile');
    }


}