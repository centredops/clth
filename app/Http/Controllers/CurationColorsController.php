<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CurationColor;
use Redirect,Response;

class CurationColorsController extends Controller
{

    public function index()
    {
        $colors = CurationColor::all();
        return view('colors.index', compact('colors'));
    }


    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'hex' => 'required'
        ],['hex.required' => 'You need to chose a color!',]);

        CurationColor::create($request->all());
        return redirect(route('colors.index'))->with('success', 'Color Added successful!');
    }


    public function edit($id)
    {

        $color = CurationColor::findOrFail($id);

        return view('colors.edit', compact('color'));
    }

    public function update(Request $request, $id)
    {

        $input = $request->all();

        $color = CurationColor::findorfail($id);


        $color->update($input);

        return redirect(route('colors.index'))->with('success', 'Color Has Been Updated');
    }


    public function destroy($id)
    {
        $color = CurationColor::findOrFail($id);
        $color->delete();

        return redirect(route('colors.index'))->with('success', 'Color Deleted!');
    }
}
