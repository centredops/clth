<?php

namespace App\Http\Controllers;

use App\HomeCapsule;
use App\Upload;
use Illuminate\Http\Request;
use File;

class HomeCapsulesController extends Controller
{
    public function index()
    {

        $details = HomeCapsule::all();

        $images = Upload::where('type', '=', 'home_capsule')->get();

        return view('home-capsules.index', compact('details', 'images'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'description' => 'required'
        ]);

        $input = $request->all();

        $contentAvailable = HomeCapsule::count();
        $updateFirstRaw = HomeCapsule::first();

        if($contentAvailable > 0){

            $updateFirstRaw->update($input);

        }else{

            HomeCapsule::create($input);
        }

        return redirect(route('home-capsules.index'))->with('success', 'Home Capsule Content Has Been Added');

    }

    public function update(Request $request)
    {
        {
            //dd($request->all());
            $image = $request->file('file');
            $fileName = $image->getClientOriginalName();
            $image->move(public_path('/assets/images/capsule-featured/'),$fileName);

            $fileUpload = new Upload();
            $fileUpload->file = $fileName;
            $fileUpload->file_id = $request->get('file_id');
            $fileUpload->type = $request->get('type');
            $fileUpload->save();
            //return response()->json(['success'=>$fileName]);
            return redirect(route('home-capsules.index'))->with('success', 'Home Capsule Image Uploaded!');
        }
    }

    public function destroy($id)
    {
        $input = HomeCapsule::findOrFail($id);

        $deleteFile = Upload::findorFail($id);

        $fileName = $deleteFile->file_id;

        if(File::exists(public_path('/assets/images/capsule-featured/') . $fileName)){
            unlink(public_path('/assets/images/capsule-featured/') . $fileName);
        }

        $input->delete();

        return redirect(route('home-capsules.index'))->with('success', 'Home Capsule Content Has Been Deleted!');
    }

    public function deleteImage($id)
    {
        $input = Upload::findOrFail($id);

        $fileName = $input->file;

        if(File::exists(public_path('assets/images/capsule-featured/') . $fileName)){
            unlink(public_path('assets/images/capsule-featured/') . $fileName);
        }
        $input->delete();

        return redirect(route('home-capsules.index'))->with('success', 'Image Deleted');
    }


}
