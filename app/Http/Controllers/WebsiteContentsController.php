<?php

namespace App\Http\Controllers;

use App\WebsiteContent;
use Illuminate\Http\Request;

class WebsiteContentsController extends Controller
{
    public function termsContent(){

        $content = WebsiteContent::where('page', 'terms-content')->get();

        return view('website-contents.terms-content', compact('content'));
    }

    public function termsCreate(Request $request){

        $input = $request->all();

        $id = $request->page_id;

        $contentAvailable = WebsiteContent::where('id', $id)->exists();
		
		if($contentAvailable){
			
			$updateContent = WebsiteContent::findOrFail($request->page_id);
			
			$updateContent->update($input);
			
		}else{
	 		WebsiteContent::create($input);
			
		}
	
        return redirect()->back()->with('success', 'Content Updated');
    }


    public function privacyContent(){

        $content = WebsiteContent::where('page', 'privacy-content')->get();

        return view('website-contents.privacy-content', compact('content'));
    }

    public function privacyCreate(Request $request){

        $input = $request->all();

        $id = $request->page_id;

        $contentAvailable = WebsiteContent::where('id', $id)->exists();

        if($contentAvailable){

            $updateContent = WebsiteContent::findOrFail($request->page_id);

            $updateContent->update($input);

        }else{
            WebsiteContent::create($input);

        }

        return redirect()->back()->with('success', 'Content Updated');
    }


    public function aboutContent(){

        $content = WebsiteContent::where('page', 'about-content')->get();

        return view('website-contents.about-content', compact('content'));
    }

    public function aboutCreate(Request $request){

        $input = $request->all();

        $id = $request->page_id;

        $contentAvailable = WebsiteContent::where('id', $id)->exists();

        if($contentAvailable){

            $updateContent = WebsiteContent::findOrFail($request->page_id);

            $updateContent->update($input);

        }else{
            WebsiteContent::create($input);

        }

        return redirect()->back()->with('success', 'Content Updated');
    }


    public function contactContent(){

        $content = WebsiteContent::where('page', 'contact-content')->get();

        return view('website-contents.contact-content', compact('content'));
    }

    public function contactCreate(Request $request){

        $input = $request->all();

        $id = $request->page_id;

        $contentAvailable = WebsiteContent::where('id', $id)->exists();

        if($contentAvailable){

            $updateContent = WebsiteContent::findOrFail($request->page_id);

            $updateContent->update($input);

        }else{
            WebsiteContent::create($input);

        }

        return redirect()->back()->with('success', 'Content Updated');
    }


    public function supportContent(){

        $content = WebsiteContent::where('page', 'support-content')->get();

        return view('website-contents.support-content', compact('content'));
    }

    public function supportCreate(Request $request){

        $input = $request->all();

        $id = $request->page_id;

        $contentAvailable = WebsiteContent::where('id', $id)->exists();

        if($contentAvailable){

            $updateContent = WebsiteContent::findOrFail($request->page_id);

            $updateContent->update($input);

        }else{
            WebsiteContent::create($input);

        }

        return redirect()->back()->with('success', 'Content Updated');
    }


    public function businessContent(){

        $content = WebsiteContent::where('page', 'business-content')->get();

        return view('website-contents.business-content', compact('content'));
    }

    public function businessCreate(Request $request){

        $input = $request->all();

        $id = $request->page_id;

        $contentAvailable = WebsiteContent::where('id', $id)->exists();

        if($contentAvailable){

            $updateContent = WebsiteContent::findOrFail($request->page_id);

            $updateContent->update($input);

        }else{
            WebsiteContent::create($input);

        }

        return redirect()->back()->with('success', 'Content Updated');
    }


}
