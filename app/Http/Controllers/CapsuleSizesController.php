<?php

namespace App\Http\Controllers;

use App\CapsuleSize;
use Illuminate\Http\Request;

class CapsuleSizesController extends Controller
{

    public function index()
    {
        $sizes = CapsuleSize::all();

        return view('capsule-sizes.index', compact('sizes'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $input = $request->all();

        CapsuleSize::create($input);

        return redirect(route('capsule-sizes.index'))->with('success', 'Size Has Been Added');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $size = CapsuleSize::findOrFail($id);

        return view('capsule-sizes.edit', compact('size'));
    }


    public function update(Request $request, $id)
    {
        $input = $request->all();

        $size = CapsuleSize::findorfail($id);


        $size->update($input);

        return redirect(route('capsule-sizes.index'))->with('success', 'Size Has Been Updated');
    }


    public function destroy($id)
    {
        $input = CapsuleSize::findOrFail($id);

        $input->delete();

        return redirect(route('capsule-sizes.index'))->with('success', 'Size Has Been Deleted!');
    }
}
