<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteController extends Controller
{

    public function homeFeaturedAdmin()
    {
        return view ('/website/admin-home-featured');
    }

    public function termsAdmin()
    {
        return view ('/website/admin-terms');
    }

    public function privacyAdmin()
    {
        return view ('/website/admin-privacy');
    }

    public function aboutAdmin()
    {
        return view ('/website/admin-about');
    }

    public function aboutUs()
    {
        return view ('/website/admin-about-us');
    }

    public function inProgress()
    {
        return view ('website.in-progress');
    }
}
