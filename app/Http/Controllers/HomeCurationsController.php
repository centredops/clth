<?php

namespace App\Http\Controllers;

use App\HomeCuration;
use Illuminate\Http\Request;
use File;

class HomeCurationsController extends Controller
{
    public function index(){

        $details = HomeCuration::all();

        return view('home-curations.index', compact('details'));
    }

    public function store(Request $request){

        $request->validate([
            'description' => 'required'
            //'file' => 'required|image'
        ]);

        $input = $request->all();

        $contentAvailable = HomeCuration::count();

        if($contentAvailable){
            return redirect(route('home-curations.index'))->with('success', 'You can only have 1 Featured Curation at a time, please delete current one before adding new one buddy!');
        }else{
            if($file = $request->file('file')){
                $fileName = strtolower( time() . '_' . $file->getClientOriginalName());;

                $file->move(public_path('assets/images/curation-featured/'),$fileName);

                $input['file'] = $fileName;
            }

            HomeCuration::create($input);
        }
        //$updateFirstRecord = HomeCuration::first();

        return redirect(route('home-curations.index'))->with('success', 'Home Curation Content Has Been Added');
   }

    public function destroy($id)
    {
        $input = HomeCuration::findOrFail($id);

        $fileName = $input->file;

        if(File::exists(public_path('assets/images/curation-featured/') . $fileName)){
            unlink(public_path('assets/images/curation-featured/') . $fileName);
        }

        $input->delete();

        return redirect(route('home-curations.index'))->with('success', 'Home Curation Content Has Been Deleted!');
    }


}
