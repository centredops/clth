<?php

namespace App\Http\Controllers;


use App\Customer;
use App\HomeCapsule;
use App\HomeCuration;
use App\Merchant;
use App\Order;
use App\Upload;
use App\WebsiteContent;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

use App \ {CurationList, CurationCategory, CurationColor, CurationType, Like};
use App \ {CapsuleList, CapsuleSize, CapsuleBrand};

class FrontController extends Controller
{
    public function index()
    {

        $homeCurations = HomeCuration::all();


        $homeCapsules = HomeCapsule::all();
        $homeSliders = Upload::where('type', '=', 'home_capsule')->get();
        return view('front.index', compact('homeCurations', 'homeCapsules', 'homeSliders'));
    }

    public function notFound()
    {
        return view('front.404');
    }

    public function curation(Request $request)
    {

        $categories = CurationCategory::all();

        $colors = CurationColor::all();

        $types = CurationType::all();



        if ($request->ajax() && isset($request->selectedCategory)) {


            $selectedCategory = $request->selectedCategory; //Category

            $curations = CurationList::whereIN('category_id', explode(',', $selectedCategory))->get();

            response()->json($curations); //return to ajax

            return view('front.curation-content', compact('curations', 'categories', 'colors', 'types'));

        }
        elseif (isset($request->selectedType)){

            $selectedType = $request->selectedType; //Type

            $curations = CurationList::whereIN('type_id', explode(',', $selectedType))->get();
            //$curations = CurationList::whereIN('type_id', explode(',', $selectedType))->latest()->paginate(3);

            response()->json($curations); //return to ajax

            return view('front.curation-content', compact('curations', 'categories', 'colors', 'types'));
        }

        elseif (isset($request->selectedColor)){

            $selectedColor = $request->selectedColor; //Color

            $curations = CurationList::whereIN('color_id', explode(',', $selectedColor))->get();;

            response()->json($curations); //return to ajax

            return view('front.curation-content', compact('curations', 'categories', 'colors', 'types'));
        }

        else {

            $curations = CurationList::latest()->get();

            return view('front.curation', compact('curations', 'categories', 'colors', 'types'));
        }

    }

    public function curationPost($id)
    {

        $curation = CurationList::findOrFail($id);

        $curation_id = $id;
        $user_id = 0;
        if (isset(auth()->user()->id)) {
            $user_id = auth()->user()->id;
        }

        $curationLiked = Like::where('curation_id', '=', $curation_id)->where('user_id', '=', $user_id)->where('like', '=', 1)->get();

        //dd($curationLiked);

        return view('front.curation-post', compact('curation', 'curationLiked'));
    }

    public function capsule()
    {

        $capsuleLists = CapsuleList::paginate(6);

        $merchants = Merchant::all();

        $featuredMerchants = Upload::where('type', '=', 'featured_merchant')->get();

        return view('front.capsule', compact('capsuleLists', 'merchants', 'featuredMerchants'));
    }

    public function capsuleDetails($id)
    {

        $capsuleGallery = Upload::where('file_id', '=', $id)->where('type', '=', 'capsule_gallery')->get();

        $capsuleDetails = CapsuleList::findOrFail($id);

        $getSizes = CapsuleList::where('id', $id)->pluck('size_id')->implode(', ', '');

        $sizes = explode(',', $getSizes);


        return view('front.capsule-details', compact('capsuleDetails', 'sizes', 'capsuleGallery'));

    }

    public function shoppingBag()
    {

        return view('front.shopping-bag');
    }

    public function addShoppingBag(Request $request, $id)
    {

        $request->validate(['size' => 'required']);

        $data = $request->all();

        //dd($data);


        $bag = session()->get('bag');

        if (!$bag) {

            $bag = [
                $id => [
                    "quantity" => 1,
                    "title" => $request->title,
                    "details" => $request->details,
                    "code" => $request->code,
                    "description" => $request->description,
                    "file" => $request->file,
                    "price" => $request->price,
                    "brand_id" => $request->brand_id,
                    "size" => $request['size']
                ]
            ];

            session()->put('bag', $bag);

            return redirect()->back()->with('success', 'Item added to your bag!!');
        }
        // if cart not empty then check if this product exist then increment quantity
        if (isset($bag[$id])) {

            $bag[$id]['quantity']++;

            session()->put('bag', $bag);

            //dd($bag);

            return redirect()->back()->with('success', 'Item added to your bag!!');

        }

        // if item not exist in cart then add to cart with quantity = 1
        $bag[$id] = [
            "quantity" => 1,
            "title" => $request->title,
            "details" => $request->details,
            "code" => $request->code,
            "description" => $request->description,
            "file" => $request->file,
            "price" => $request->price,
            "brand_id" => $request->brand_id,
            "size" => $request->size
        ];

        session()->put('bag', $bag);

        //dd($bag);

        return redirect()->back()->with('success', 'Item added to your bag!!');

    }

    public function updateShoppingBag(Request $request)
    {
        if ($request->id and $request->quantity) {
            $bag = session()->get('bag');

            $bag[$request->id]["quantity"] = $request->quantity;

            session()->put('bag', $bag);

            session()->flash('success', 'Shopping bag updated successfully');
        }
    }

    public function removeShoppingBag(Request $request)
    {
        if ($request->id) {

            $bag = session()->get('bag');

            if (isset($bag[$request->id])) {

                unset($bag[$request->id]);

                session()->put('bag', $bag);
            }

            session()->flash('success', 'Item removed from Bag');
        }
    }

    public function checkoutBag()
    {

        $user_id = auth()->user()->id;

        $userAlready = Customer::where('user_id', '=', $user_id)->exists();

        if ($userAlready) {

            $customer_details = Customer::where('user_id', $user_id)->get();

        } else {

            $input = new Customer([
                'user_id' => $user_id
            ]);

            $input->save();
        }

        $customer_details = Customer::where('user_id', $user_id)->get();

        return view('front.checkout-bag', compact('customer_details'));
    }

    public function placeOrder(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'city' => 'required',
            'postal_code' => 'required',
            'email' => 'email',
            'cell' => 'required|numeric',
            'payment_method' => 'required',
            'address' => 'required',
        ]);

        $input = $request->all();

        // dd($input);

        $userAlready = Customer::where('user_id', '=', $id)->first();

        //dd($userAlready);

        $userAlready->update($input);


        //Create Order

        $bag = session()->get('bag');

        $total = 0;
        $products = [];
        $orderNumber = strtoupper($request->cell . substr(md5(time()), 0, 8));
        //dd($orderNumber);
        $payment_method = $request['payment_method'];
        foreach ($bag as $data) {
            $row = [];
            $row['user_id'] = $id;
            $row['order_number'] = $orderNumber;
            $row['file'] = $data['file'];
            $row['size'] = $data['size'];
            $row['title'] = $data['title'];
            $row['details'] = $data['details'];
            $row['code'] = $data['code'];
            $row['description'] = $data['description'];
            $row['quantity'] = $data['quantity'];
            $row['total'] = $data['price'] * $data['quantity'];
            $row['payment_method'] = $payment_method;
            $row['created_at'] = Carbon::now();
            $row['updated_at'] = Carbon::now();
            $products[] = $row;  //add row
        }

        Order::insert($products);

        //Send Mail

        $data = array(
            //'campaign' => 'Spin and Win',
            'company' => 'CLTH',
            'email' => auth()->user()->email,
            'sender' => 'webmaster@clth.co.za',
            'order_number' => $orderNumber,
            'order_date' => Carbon::now(),
            'bag' => $bag = session()->get('bag')
        );
        Mail::send('front.order-mail', $data, function ($message) use ($data) {
            $message->to($data['email'])
                ->subject('Capsule Order' . ' - ' . $data['order_number']);
            $message->from($data['sender'], $data['company']);
        });


        return redirect(route('front.order-details'));

    }

    public function orderDetails(){

        //session()->forget('bag');

        //session_destroy();

        return view('front.order-details');
    }

    public function continueShopping(){

        session()->forget('bag');

        return redirect(route('front.capsule'));

    }

    public function trackOrder(){

        session()->forget('bag');

        return redirect(route('front.shopping-closet'));

    }

    public function placeOrderMail()
    {

    }

    public function serviceTerms()
    {
        $content = WebsiteContent::where('page', 'terms-content')->get();

        return view('front.service-terms', compact('content'));
    }

    public function privacyPolicy()
    {
        $content = WebsiteContent::where('page', 'privacy-content')->get();

        return view('front.privacy-policy', compact('content'));
    }

    public function business()
    {
        $content = WebsiteContent::where('page', 'business-content')->get();

        return view('front.business', compact('content'));
    }

    public function aboutUs()
    {
        $content = WebsiteContent::where('page', 'about-content')->get();

        return view('front.about-us',compact('content'));
    }

    public function contactUs()
    {
        $content = WebsiteContent::where('page', 'contact-content')->get();

        return view('front.contact-us', compact('content'));
    }

    public function support()
    {
        $content = WebsiteContent::where('page', 'support-content')->get();

        return view('front.support', compact('content'));
    }
}
