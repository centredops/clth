<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Users;

use App\Role;
use Closure;

class DashboardAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userRole = Role::pluck('id');

        $currentUser = Auth::user()->role_id;

      if(2 == $currentUser){
            return redirect('/')->with('success', 'You have no access to this section');
        }

        return $next($request);
    }
}
