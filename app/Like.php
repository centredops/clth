<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ['curation_id', 'user_id', 'like'];

    protected $primaryKey = "curation_id";

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function curation(){
        return $this->belongsTo('App\CurationList', 'curation_id');
    }

}


