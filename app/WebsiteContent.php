<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteContent extends Model
{
    protected $fillable = ['page_id', 'page', 'content'];

    protected $primaryKey = "page_id";
}
