<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurationList extends Model
{
    protected  $fillable = [

        'title',
        'description',
        'file',
        'links',
        'category_id',
        'type_id',
        'color_id',
        'is_active'
    ];

    public function category(){
        return $this->belongsTo('App\CurationCategory')->withDefault();
    }

    public function color(){
        return $this->belongsTo('App\CurationColor')->withDefault();
    }

    public function type(){
        return $this->belongsTo('App\CurationType')->withDefault();
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
