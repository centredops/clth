<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CapsuleBrand extends Model
{
    protected $fillable = ['name'];
}
