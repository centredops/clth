<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeCuration extends Model
{
    protected $fillable = ['description', 'file'];
}
