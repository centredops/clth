<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurationCategory extends Model
{
    protected $fillable = ['name', 'description'];
}
